<div class="modal" id="passwordModal">

    <div class="modal--container">
        <div class="modal--container__head">
            <div id="userAgreement" style="display: block">
                <h2>Brukeravtale</h2>
            </div>
            <div id="welcome-message" style="display: none">
                <h2>Velkommen som bruker.</h2>
                <h4>Du trenger et passord.</h4>
            </div>
        </div>
        <form class="modal--container__form fill" method="post" action="api/public/firstTimeLogin" id="updatePasswordForm">
            <div id="agreement-box" style="display: block">
                <div class="agreement-text">
                    <h1> Personvernerklæring</h1>
                    <br>
                    Denne personværnerkleringen forteller om hvordan 'Diamond Domain' samler inn og bruker dine persondata.
                    Erklæringen inneholder opplysninger du har krav på når det samles inn opplysninger fra nettstedet vårt (personopplysningsloven § 19) 
                    og generell informasjon om hvordan vi behandler personopplysninger (personopplysningsloven § 18 1. ledd).
                    <br>
                    <br>
                    <h2>Behandling av opplysninger</h2>
                    <br>
                    Siden driftes i sin helet av 'Diamond Domain', tjenesten er levert av Morild AS.
                    Personer som ønsker å benytte seg av tjenesten vil bli lagt inn i systemet med fullt navn og epostadresse av Administrator. 
                    Data som samles inn vil bli lagret på en server som er driftet av UiT - Norges Arktiske Universitet.
                    <br>
                    <br>
                    <h2>Bruk av tjenesten</h2>
                    <br>
                    Dersom du legger inn et varsel vil det bli lagret på en server driftet av UiT - Norges Arktiske Universitet. Det er kun Administrator som har tilgang til denne databasen, hvor dine data lagres i klartekst.

                    Administrator vil ha komplett innsyn i alle varsel som blir rapportert inn. Dersom bruker har valgt å varsle anonymt vil Administrator se navn i klartekst. Dersom Administrator ser på et varsel vil dette bli loggført i våre systemer. 

                    De ansatte som til daglig jobber med å løse sakene som rapporteres inn ved hjelp av tjenesten vil få innsyn i varsler som er rapportert til dem, eller deres grupper. Informasjon som inngår i et varsel er: brukernavn og varselet i klartekst.
                    Dersom bruker av tjenesten har valgt å varsle anonymt, vil ikke de ansatte kunne se brukernavn, og må ta saken videre til administrator dersom det anses at saken er alvorlig.
                    <br>
                    <br>

                    <h2>E-post adresse</h2>
                    <br>
                    E-post adresse benyttes til å sende varsel til varsler dersom det skjer endringer på varselet.
                    Dersom det besluttes at et aktuelt varsel er alvorlig, kan epostadresse bli hentet ut for å kontakte vedkommende.
                </div>

                <div class="agreement-checkbox">  Godta
                    <input type="checkbox" id="mycheckbox" class="agreement-checkbox-box" onclick="agreementRead()"/>
                </div>
            </div>
            <div id="hidden_before_agreed" style="display: none">
                <input type="hidden" name="userid" class="popuserid" />
                <p>Nytt passord</p>
                <input class="wide" name="password" id="pw1" type="password" required placeholder="Nytt passord her">

                <p>Bekreft passord</p>
                <input class="wide" name="confirmpassword" id="pw2" type="password" required
                       placeholder="Gjenta passord her">

                <div class="submit-button-container">
                    <button type="submit" class="submit-button">
                        Endre
                        <img class="img-small" src="imgs/send.svg" alt="send">
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function agreementRead() {

        var checkbox = document.getElementById("mycheckbox");
        var agreementBox = document.getElementById("agreement-box");
        var content = document.getElementById("hidden_before_agreed");
        var welcomeMessage = document.getElementById("welcome-message");

        if (checkbox.checked === true) {
            content.style.display = "block";
            welcomeMessage.style.display = "block";
            agreementBox.style.display = "none";
            document.getElementById("userAgreement").style.display="none";
        } else {
            content.style.display = "none";

        }
    }
</script>