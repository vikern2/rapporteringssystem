<div class="view_event_container" id="view_event_container">

    <input type="hidden" name="complaint_id" id="complaint_id"/>

    <div class="list-head fixed">
        <div class="head-info height_100">
            <div class="hide-btn"><img class="img-small"
                                       src="imgs/close_w<?php if ($userlevel == 3) echo "_admin" ?>.svg"/></div>
            <div class="head-title"></div>
            <div class="head-author"></div>
            <div class="head-status"></div>
            <div class="head-options-icon" id="deleteComplaintBtn">
                <img class="img-small" src="imgs/delete.svg" alt="delete">
            </div>
            <?php if ($userlevel == 2) { ?>
                <div class="head-options-icon" id="process_event">
                    <img class="img-small" src="imgs/gavel.svg" alt="process">
                </div>
            <?php } ?>
        </div>
    </div>
    <?php
    if ($userlevel == 3) {
        include 'view_sneakpeak.php';
    }
    ?>

    <div class="event_container__main" id="complaint_display">

        <div class="event_container__user_view" id="container__user_view">

            <div class="event_container__row">
                <div class="row__title">
                    <h3>Tidspunkt:</h3>
                </div>
                <div class="row__field" id="view_event_container__row_time">
                    <h3>13:37 13.3.7</h3>
                </div>
            </div>

            <div class="event_container__row">
                <div class="row__title">
                    <h3>Varsler:</h3>
                </div>
                <div class="row__field" id="view_event_container__row_submitter">
                </div>
            </div>

            <div class="event_container__row">
                <div class="row__title">
                    <h3>Status:</h3>
                </div>
                <div class="row__field" id="view_event_container__row_status">
                </div>
            </div>

            <div class="event_container__row"></div>

            <div class="event_container__row">
                <div class="row__title full_width" id="view_event_container__row_category">
                </div>
            </div>


            <div class="row__field content border-top padding_16_32" id="view_event_container__row_content">
            </div>

            <div class="event_container__row">
                <div class="row__title full_width">
                    <h2>Tilbakemelding</h2>
                </div>
            </div>


            <div class="row__field content border-top padding_16_32" id="view_event_container__row_user_feedback">
            </div>

        </div>


        <?php if ($userlevel >= 2) { ?>

            <div class="event_container__hr_view hidden" id="container__hr_view">

                <form method="post" id="commentForm" action="api/public/submitComment">
                    <input type="hidden" name="data-id" id="view_event_container__id" class="use_false"/>
                    <input type="hidden" name="userid" class="popuserid use_false"/>

                    <div class="event_container__user_view">

                        <div class="event_container__row">
                            <div class="row__title full_width barrier">
                                <h3>Skjema for internt bruk</h3>
                            </div>
                        </div>
                        <div class="event_container__row">
                            <div class="row__title">
                                <h3>Endre status:</h3>
                            </div>
                            <div class="row__field width_70 height_100">
                                <select class="full_width" required name="newStatus" id="submitformCategory">
                                    <option value="" disabled selected hidden>Trykk for å velge..</option>
                                    <?php
                                    foreach ($statuses as $status) {
                                        echo '<option value="' . $status['id'] . '" name="' . $status['id'] . '"><h3>' . $status['text'] . '</h3></option>';
                                    } ?>
                                </select>
                            </div>
                        </div>

                        <div class="event_container__row">
                            <button type="submit" id="complaint_submitFeedback" class="btn-img submit-button"
                                    name="submit-feedback">Lagre endringer <img class="img-small" src="imgs/send.svg"
                                                                                alt="send"></button>
                        </div>
                        <div class="event_container__row"></div>

                        <div class="event_container__row">
                            <div class="row__title full_width">
                                <h2>Interne Kommentarer</h2>
                            </div>
                        </div>


                        <div class="row__field content border-top">
                        <textarea class="use_false"
                                  placeholder="Dette vises ikke for varsler"
                                  id="ta_internal view_event_container__row_hr_feedback_internal" name="content"
                                  rows="8"
                                  cols="50" title="Avvik"></textarea>
                        </div>

                        <div class="event_container__row">
                            <div class="row__title full_width">
                                <h2>Tilbakemelding</h2>
                            </div>
                        </div>


                        <div class="row__field content border-top" id="view_event_container__row_hr_feedback">
                        <textarea class="use_false" placeholder="Tilbakemelding til varsler" id="ta_feedback_hr"
                                  name="kommentarer" rows="8"
                                  cols="50" title="Avvik"></textarea>
                        </div>
                    </div>
                </form>
            </div>
        <?php } ?>

    </div>
</div>