<div id="editVarselCard" class="submit-form">

    <!-- header -->
    <div class="list-head height_5">
    <!-- header options bar -->
    <div class="head-info height_100">
        <div class="head-date hide-btn"><img class="img-small" src="imgs/close_w.svg"/></div>
    </div>
    </div>

    <form class="height_95" id="editForm" method="post" action="api/public/editVarsel" >
        <input type="hidden" name="id" id="editid" class="use_false" value="<?php  ?>" />
        <div class="event_container__row">
            <div class="row__title">
                <h3>Kategori</h3>
            </div>
            <select required name="cat" id="editformCategory">
                <option value="" disabled selected hidden>..Trykk for å velge</option>
                <?php
                foreach ($categories as $category) {
                    echo '<option value="' . $category['id'] . '" name="' . $category['id'] . '"><h3>' . $category['text'] . '</h3></option>';
                }
                ?>
            </select>
        </div>
        <div class="event_container__row">
            <div class="row__title">
                <h3>Mottaker</h3>
            </div>
            <select required name="receiver" id="editformReceiver">
                <option value="" disabled selected hidden>..Trykk for å velge</option>
                <?php
                foreach ($recievers as $receiver) {
                    if ($receiver['id'] != $userid) {
                        echo '<option value="' . $receiver['id'] . '" name="' . $receiver['id'] . '"><h3>' . $receiver['name'] . '</h3></option>';
                    }
                }
                ?>
            </select>
        </div>
        <div class="submit-textfield">
            <div class="row__title">
                <h3>Beskrivelse</h3>
            </div>
            <textarea id="editformTextarea" name="content" rows="8" cols="50" title="Avvik"></textarea>
            <br>
        </div>

        <div class="submit-button-container">
            <button type="submit" id="submitEditForm" class="centered_button btn-img submit-button" name="submit-varsel">Lagre endring <img class="img-small" src="imgs/gavel.svg" alt="process"></button>
        </div>

    </form>
</div>