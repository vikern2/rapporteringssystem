<?php if ($userlevel != 3) { ?>
    <div class="home-activity-card" id="new-form">
        <div class="activity-new-event">
            <img class="activity-icon" src="imgs/new-form.svg" alt="exit">
        </div>
        <div class="activity-card-description">
            <h2>Registrer nytt varsel</h2>
        </div>
    </div>

    <div class="home-activity-card" id="list-user">
        <div class="activity-new-event">
            <img class="activity-icon" src="imgs/list2.svg" alt="exit">
        </div>
        <div class="activity-card-description">
            <h2>Se mine varslinger</h2>
        </div>
    </div>
<?php }

if ($userlevel > 1) {
    ?>
    <div class="home-activity-card" id="list-all">
        <div class="activity-new-event">
            <img class="activity-icon" src="imgs/list.svg" alt="exit">
        </div>
        <div class="activity-card-description">
            <h2>Se alle varsel</h2>
        </div>
    </div>
<?php } ?>

<div class="home-activity-card" id="main_menu__settings">
    <div class="activity-new-event">
        <img class="activity-icon" src="imgs/symbol_settings.svg" alt="settings">
    </div>
    <div class="activity-card-description">
        <h2>Instillinger</h2>
    </div>
</div>
<?php if ($userlevel != 3) { ?>
    <div class="home-activity-card hidden" id="main_menu__update_user_data">
        <div class="activity-new-event">
            <img class="activity-icon" src="imgs/contacts.svg" alt="person instillinger">
        </div>
        <div class="activity-card-description">
            <h2>Endre Persondata</h2>
        </div>
    </div>
<?php } ?>
<?php
if ($userlevel == 3) {
    ?>
    <div class="home-activity-card hidden" id="settings_add_user">
        <div class="activity-new-event">
            <img class="activity-icon" src="imgs/person_add.svg" alt="Icon for user">
        </div>
        <div class="activity-card-description">
            <h2>Legg til ny bruker</h2>
        </div>
    </div>

    <div class="home-activity-card hidden" id="settings_category">
        <div class="activity-new-event">
            <img class="activity-icon" src="imgs/add_category.svg" alt="Icon for groups">
        </div>
        <div class="activity-card-description">
            <h2>Administrer Kategorier</h2>
        </div>
    </div>
    <?php
}
?>

<div class="home-activity-card hidden" id="main_menu__settings_exit">
    <div class="activity-new-event">
        <img class="activity-icon" src="imgs/back.svg" alt="instillinger">
    </div>
    <div class="activity-card-description">
        <h2>Tilbake</h2>
    </div>
</div>

<?php if ($userlevel > 1) { ?>
    <div class="home-activity-card" id="statistics">
        <div class="activity-new-event">
            <img class="activity-icon" src="imgs/line-chart.svg" alt="exit">
        </div>
        <div class="activity-card-description">
            <h2>Statistikker</h2>
        </div>
    </div>
<?php } ?>

<div class="home-activity-card" id="logout">
    <div class="activity-exit">
        <img class="activity-icon" src="imgs/lock-lock.svg" alt="exit">
    </div>
    <div class="activity-card-description">
        <h2>Logg ut</h2>
    </div>
</div>

