<div class="list-main" id="list-all-div">

    <div class="list-head height_60" id="hr-list-head">

        <div class="head-info" id="hr_list_top_bar">
            <div class="head-date hide-btn"><img class="img-small" src="imgs/close_w.svg"/></div>
            <div class="head-title"></div>
            <div class="head-author"></div>
            <div class="head-status"></div>
            <div class="head-options-icon">
                <img id="settings_hr" src="imgs/symbol_settings.svg" alt="filter options">
            </div>
        </div>

        <div class="head-options hidden" id="hr_options">
            <div class="head-info" id="hr-filter-info">
                <div class="head-date smallest">Filtrer</div>
                <div class="head-title smaller">
                    <select required name="filterType" id="filterType_hr">
                        <option value="" disabled selected hidden>..Velg type</option>
                        <option value="category" name="category">Kategori</option>
                        <option value="user" name="user">Mottaker</option>
                        <option value="status" name="status">Status</option>
                    </select>
                </div>
                <div class="head-author smaller">
                    <select required name="filterValue" id="filterValue_hr">
                        <option value="" disabled selected hidden class="donthide">liste basert på type</option>
                        <?php
                        foreach ($categories as $category) {
                            echo '<option value="' . $category['text'] . '" name="' . $category['id'] . '" class="option--category hidden"><h3>' . $category['text'] . '</h3></option>';
                        } ?>
                        <?php
                        foreach ($usertypes as $receiver) {
                            if ($receiver['id'] != 1) {
                                echo '<option value="' . $receiver['description'] . '" name="' . $receiver['description'] . '" class="option--user hidden"><h3>' . $receiver['description'] . '</h3></option>';
                            }
                        } ?>
                        <?php
                        foreach ($progresslist as $progresstype) {
                            echo '<option value="' . $progresstype['text'] . '" name="' . $progresstype['id'] . '" class="option--status hidden"><h3>' . $progresstype['text'] . '</h3></option>';
                        } ?>
                    </select>
                </div>
                <button id="clearFilters_hr">Clear</button>
            </div>

            <div class="head-info" id="hr-sort-info">
                <div class="head-date smallest">Sortering</div>
                <div class="head-title smaller">
                    <select required name="cat" id="sortType_hr">
                        <option value="date" name="date" selected disabled><h3>Dato</h3></option>
                    </select>
                </div>
                <div class="head-author smaller">
                    <select required name="cat" id="sortValue_hr">
                        <option value="" disabled selected hidden>liste basert på type</option>
                        <option value="asc" name="asc"><h3>Minkende</h3></option>
                        <option value="desc" name="desc"><h3>Økende</h3></option>
                    </select></div>
            </div>

        </div>

        <div class="head-info" id="hr_list_info">
            <div class="head-date">Dato</div>
            <div class="head-title">Kategori</div>
            <div class="head-author">Rapporert av</div>
            <div class="head-status">Status</div>
        </div>
    </div>


    <div class="list-feed" id="hrList">

        <?php
        foreach ($received as $item) {
            ?>
            <div class="list-item" 
                data-id="<?php echo $item->getId(); ?>"
                data-cat="<?php echo $item->getCategory(); ?>"
                data-submitid="<?php echo $item->getId()?>"
                data-content="<?php echo $item->getContent(); ?>"
                data-submitterid="<?php echo $item->getSubmitterid(); ?>" 
                data-status="<?php echo $item->getStatus(); ?>"
                data-time="<?php echo $item->getTime(); ?>" 
                data-feedback='<?php echo json_encode($item->getFeedback()); ?>'
                data-anon="<?php echo $item->getAnon(); ?>"
                data-submittername="<?php if (!$item->getAnon())echo $item->getSubmittername(); ?>">
                <div class="list-content">
                    <div class="list-date"><?php
                        echo explode(' ', $item->getTime())[0];
                        ?></div>
                    <div class="list-title"><?php
                        echo $item->getCategory();
                        ?></div>
                    <div class="list-author">
                        <?php
                        if ($item->getAnon()) {
                            if ($userlevel > 2) {
                                echo $item->getSubmittername();
                            } else {
                                echo 'Annonymt';
                            }
                        } else {
                            echo $item->getSubmittername();
                        }
                        ?>
                    </div>
                    <div class="list-status"><?php
                        echo $item->getStatus();
                        ?></div>
                </div>
                <div class="list-functions">
                    <div class="list-fc-view">
                        <img class="list-icon <?php if ($userlevel ==3) echo "admin_view";
                        elseif ($userlevel == 2) echo "hr_view"?>" src="imgs/view.svg" alt="view">
                    </div>
                </div>
            </div>
            <?php
        };
        ?>
    </div>

</div>