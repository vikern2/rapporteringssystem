<?php
$groupdisplayed = [];
$categories = $helper->getCategories()['data'];
?>

<div id="varselCard" class="submit-form">
    <!-- header -->
    <div class="list-head">
        <!-- header options bar -->
        <div class="head-info">
            <div class="hide-btn"><img class="img-small" src="imgs/close_w.svg"/></div>
        </div>
    </div>

    <form id="newComplaintForm" method="post" style="height: auto!important" action="api/public/newVarsel" >
        <input type="hidden" name="userid" class="popuserid" id="userid" class="use_false" />
        <div class="event_container__row">
            <div class="row__title">
                <h3>Kategori:</h3>
            </div>
            <select required name="cat" id="submitformCategory">
                <option value="" disabled selected hidden>Trykk for å velge</option>
                <?php
                foreach ($categories as $category) {
                    echo '<option value="' . $category['id'] . '" name="' . $category['id'] . '"><h3>' . $category['text'] . '</h3></option>';
                }
                ?>
            </select>
        </div>
        <div class="event_container__row border-top">
            <div class="row__title">
                <h3>Mottaker:</h3>
            </div>
            <select required name="receiver" id="submitformReceiver">
                <option value="" disabled selected hidden>Trykk for å velge</option>
                <?php
                foreach ($recievers as $receiver) {
                    if (!in_array($receiver['groupname'], $groupdisplayed)) {
                        array_push($groupdisplayed, $receiver['groupname']);
                        echo '<option value="' . $receiver['id'] . '_' . $receiver['groupname'] . '" name="' . $receiver['id'] . '_' . $receiver['groupname'] . '"><h3>Gruppen ' . $receiver['groupname'] . '</h3></option>';
                    }
                    if ($receiver['id'] != $userid) {
                        echo '<option value="' . $receiver['userid'] . '" name="' . $receiver['userid'] . '"><h3>' . $receiver['name'] . '</h3></option>';
                    }
                }
                ?>
            </select>
        </div>
        <div class="submit-textfield border-top">
            <div class="row__title">
                <h3>Beskrivelse:</h3>
            </div>
            <textarea placeholder="Beskriv hendelse" id="submitformTextarea" name="content" rows="8" cols="50" title="Avvik"></textarea>
            <br>
        </div>

        <div class="event_container__row">
            <div class="md-checkbox">
                <input type="checkbox" name="ignore" class="use_false" id="1">
                <label for="1">Jeg har lest og forstått <a href="#" onclick="show_gdpr()">vilkårene</a> for å benytte tjenesten</label>
            </div>
        </div>
                
        <div class="event_container__row">
            <div class="md-checkbox">
                <input type="checkbox" name="anon" id="anon">
                <label for="anon">Varsle annonymt</label>
            </div>
        </div>
        <div class="event_container__row">
                <button disabled type="submit" id="submitForm"class="centered_button btn-img submit-button" name="submit-varsel">Send inn <img class="img-small" src="imgs/send.svg" alt="send"></button>
        </div>

    </form>
</div>