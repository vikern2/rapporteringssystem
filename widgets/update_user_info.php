<div id="user_contact_info" class="submit-form">

    <div class="list-head">
        <div class="head-info height_100">
            <div class="hide-btn"><img class="img-small" src="imgs/close_w.svg"/></div>
        </div>
    </div>

   

    <div class="event_container__row" id="settings_row__change_name">
        <div class="row__title full_width">
            <h3>Endre Brukernavn:</h3>
        </div>
    </div>

    
    <form id="changeUsernameForm" class="event_container__row full_width" method="post" action="api/public/changeUsername" style="flex-direction: column;">
        <div class="event_container__row full_width" id="container__change_name">
            <input type="hidden" name="userid" id="userid" class="use_false popuserid" />
            <input type="text" placeholder=<?php echo($user->getUsername()) ?> class='width_70 margin_left_10 list-content__input' name='new_username' id='input__new_username'>
            <button type="submit" class="height_90 margin_0_auto centered_button">Oppdater</button>  
        </div>
    </form>
    <div class="event_container__row border-top" id="settings_row__change_email">
        <div class="row__title full_width">
            <h3>Endre Epost-adresse:</h3>
        </div>
    </div>

    
    <form id="changeEmailForm" class="event_container__row full_width" method="post" action="api/public/changeEmail" style="flex-direction: column;">
        <div class="event_container__row full_width" id="container__change_email">
            <input type="hidden" name="userid" id="userid" class="use_false popuserid" />
            <input type="text" placeholder="<?php echo($user->getEmail())?>" class='width_70 margin_left_10 list-content__input' name='new_email' id='input__new_email'>
            <button type="submit" class="height_90 margin_0_auto centered_button">Oppdater</button>    
        </div>
    </form>
    <div class="event_container__row border-top" id="settings_row__change_password">
        <div class="row__title full_width">
            <h3>Passord:</h3>
        </div>
        
    </div>

    <form id="changePasswordForm" method="post" class="event_container__row full_width" action="api/public/changePassword" style="height: 120px;flex-direction: column;" >
        <div class="event_container__row full_width" id="container__change_password">
            <input type="hidden" name="userid" id="userid" class="use_false popuserid" />
            <input type="password" placeholder="Skriv inn gjeldende passord" class='width_70 margin_left_10 list-content__input' name='old_password' id='input_old_password' oninput='validateOldPassword()'>
        </div>

        <div class="event_container__row" id="new_password1">
            <input type="password" placeholder="Nytt passord" class='width_70 margin_left_10 list-content__input' name='new_password' id='input__new_password'>
        </div>

        <div class="event_container__row" id="new_password2">
            <input type="password" placeholder="Bekreft nytt passord" class='width_70 margin_left_10 list-content__input' name='new_password_confirm' id='input__new_password_confirm'>
            <button type="submit" class="height_90 margin_0_auto centered_button">Oppdater</button>
            
        </div>
    </form>
</div>