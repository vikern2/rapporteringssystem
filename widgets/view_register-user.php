<?php

$usertypes = $helper->getUserType()['data'];
?>
<div id="add_user_div" class="submit-form">

    <div class="list-head">
        <div class="head-info">
            <div class="head-date hide-btn"><img class="img-small" src="imgs/close_w.svg"/></div>
        </div>
    </div>

    <form class="height_95" id="newUserForm" method="post" action="api/public/newUser">

        <div class="event_container__row">
            <div class="row__title">
                <h3>Brukertype:</h3>
            </div>
            <select required name="usertypeid" id="usertypeid">
                <option value="" selected disabled hidden>Velg kategoritype</option>
                <?php
                foreach ($usertypes as $category) {
                    echo '<option value="' . $category['id'] . '" name="' . $category['description'] . '"><h3>' . $category['description'] . '</h3></option>';
                }
                ?>
            </select>
        </div>

        <div class="event_container__row">
            <div class="row__title">
                <h3>Navn:</h3>
            </div>
            <input class="width_70" name="employeename" type="text" placeholder="Ansattesnavn" id="employeename" required/>
        </div>

        <div class="event_container__row">
            <div class="row__title">
                <h3>Email:</h3>
            </div>
            <input class="width_70" name="employeename" type="text" placeholder="Email" id="employeeemail" required/>
        </div>

        <div class="event_container__row">
            <div class="row__title">
                <h3>Brukernavn:</h3>
            </div>
            <input class="width_70" name="username" type="text" placeholder="Brukernavn" id="username" required/>
        </div>

        <div class="event_container__row">
            <div class="row__title full_width">
                <p>Bruker må endre passord ved første gangs login. Passord blir satt til "metoo"</p>
            </div>
        </div>

        <!--Egen div for passord må implementeres om passord skal være en del av registrer bruker  i helper-->
        <div name="password" id="password" data-value="1"></div>

        <div class="submit-button-container">
            <button type="submit" id="submitForm-registerUser" class="centered_button submit-button">
                Registrer
                <img class="img-small" src="imgs/send.svg" alt="send">
            </button>
        </div>
    </form>
</div>