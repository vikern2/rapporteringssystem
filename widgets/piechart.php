<div id="statistics-and-reports" class="submit-form">

    <!-- header -->
    <div class="list-head">
        <!-- header options bar -->
        <div class="head-info">
            <div class="head-date hide-btn"><img class="img-small" src="imgs/close_w.svg"/></div>
        </div>
    </div>
        <?php


        $submitscounters = array();
        $submitStatus = array();
        $index = 1;

        $categories = $helper->getCategories()['data'];
        $submits = $helper->getAllVarsler()['data'];
        $firstVarselDate = DateTime::createFromFormat('Y-m-d H:i:s', $helper->getFirstVarselTime()['data']['time']);
        $firstVarselDate = $firstVarselDate->format('m');
        $todayMonth = date("m", mktime());
        $divider = $todayMonth-$firstVarselDate+1;

        $progresses = array(array($progresslist[0]['text'], 1), array($progresslist[1]['text'], 2), array($progresslist[2]['text'], 3), array($progresslist[3]['text'], 4));
        $submitStatus = array(array('Status', 'Antall'), array($progresslist[0]['text'], 0), array($progresslist[1]['text'], 0), array($progresslist[2]['text'], 0), array($progresslist[3]['text'], 0));
        $noSubmits = 0;

        $submitStatus[0] = ['Status', 'Antall'];
        $submitscounters[0] = ['Category', 'Number Of Submits'];
        $worstCategory = [0, 'Ingen'];

foreach($categories as $category){
        $varselCount = $helper->getCategorySpecificVarselCount($category['id']);
        $monthVarselCount = $helper->getMonthCategorySpecificVarselCount($category['id'], $todayMonth-1);

        if($varselCount != null) {
            $submitscounters[$index] = array($varselCount->getCategoryText(), $varselCount->getNumberOfSubmits());
            $index++;
        }
}
    foreach($submits as $varsel){
        $index = 1;
        $noSubmits++;
        foreach($progresses as $progress) {
            if ($varsel->getStatus() == $progress[0]){
                $submitStatus[$index][1]++;
                $index = 1;
            }else{
                $index++;
            }
        }
    }

foreach(array_slice($submitscounters,1) as $submitscounter){
        if($submitscounter[1] > $worstCategory[0]){
            $worstCategory = [$submitscounter[1], $submitscounter[0]];
        }
}
        $notProcessed = 100 - $submitStatus[1][1]/$noSubmits*100;
        $submitscounters = json_encode($submitscounters);
        $submitStatus = json_encode($submitStatus);
?>
    <div id="general_info">
        <div id="no_submits">
            <?php
                echo 'Antall varsler';
            ?>
            <div id="no_submits_result">
            <br>
            <?php
                echo $noSubmits;
             ?>
            </div>
        </div>
        <div id="not_processed">
            <?php
                echo 'Ikke ferdig behandlet';
            ?>
            <div id="not_processed_result">
            <br>
            <?php
                echo $notProcessed . '%';
            ?>
            </div>
        </div>
        <div id="worst_category">
            <?php
                echo 'Kategori med flest varsler';
                ?>
            <div id="worst_category_result">
            <br>
            <?php
                echo $worstCategory[1];
            ?>
            </div>
        </div>
    </div>

    <html>

    <script type="text/javascript">

        google.charts.load('current', {'packages':['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawChart1);
        google.charts.setOnLoadCallback(drawChart2);

        function drawChart1() {
            var data = google.visualization.arrayToDataTable(
                <?php echo ($submitscounters); ?>
            );

            var options = {
                title: 'Varsler',
                chartArea: {width: '75%', height: '75%'}
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart_div'));

            chart.draw(data, options);

        }

        function drawChart2(){
            var data = google.visualization.arrayToDataTable(
                <?php echo ($submitStatus); ?>
            );

            var options = {
                title: 'Status',
                chartArea: {width: '75%', height: '75%'}
            };

            var chart = new google.visualization.PieChart(document.getElementById('colchart_div'));

            chart.draw(data, options);
        }
    </script>
<div id="chart_container">
    <div id="piechart_div"></div>
    <div id="colchart_div"></div>
</div>

    </html>
<?php
$todayMonth = date("m", mktime());

$lastMonthVarsler = $helper->getMonthSpecificVarsler($todayMonth-1);
$monthMinusTwoVarsler = $helper->getMonthSpecificVarsler($todayMonth-2);


    //$categories = $helper->getCategories();
$categoryCount = count($categories);
$catArrayCounter = array_fill(0, $categoryCount, 0);
$index = 0;

if($lastMonthVarsler != null && $monthMinusTwoVarsler != null){
    $lastMonthCount = count($lastMonthVarsler['data']);
    $monthMinusTwoVarslerCount = count($monthMinusTwoVarsler['data']);

    foreach($categories as $category){
        foreach ($lastMonthVarsler['data'] as $varsel){
            if($varsel['category'] == $category['id']){
                $catArrayCounter[$index]++;
            }
        }$index++;
    }

    $categoryWithMostVarsels = array_search(max($catArrayCounter), $catArrayCounter);

    $compareVarselCount = 0;
    foreach ($monthMinusTwoVarsler['data'] as $varsel){
        if($varsel['category'] == $categories[$categoryWithMostVarsels]){
            $compareVarselCount++;
        }
    }

    if($compareVarselCount > max($catArrayCounter)){
        $moreOrLessString = " redusert fra de ";
    }else{
        $moreOrLessString = " en økning fra de ";
    }

    if($lastMonthCount > $monthMinusTwoVarslerCount){
        $percentageDifference = (int)(($lastMonthCount/$monthMinusTwoVarslerCount)/$monthMinusTwoVarslerCount * 100 +.5);
        $incOrDec = " flere";
    }else{
        $percentageDifference = (int)(($lastMonthCount/$monthMinusTwoVarslerCount)/$lastMonthCount * 100 +.5);
        $incOrDec = " mindre";
    }
setlocale (LC_TIME, 0);
    ?>

<div id="reports" style="padding-left:6%; padding-right: 6%;">
    <h1 style="font-size: 20px; padding-bottom: 1%;"><?php echo (date('F', mktime(0, 0, 0, $todayMonth-1, 1))) ?> </h1>
    <?php
    echo "I " . (date('F', mktime(0, 0, 0, $todayMonth-1, 1))) . " mottok vi " . $lastMonthCount . " varsler. Dette er " . $percentageDifference .  "% " . $incOrDec . " enn i " . (date('F', mktime(0, 0, 0, $todayMonth-2, 1))) . " måned.";
    ?>
    <br>
    <?php
    echo " Kategorien med flest varsler var " .  $categories[$categoryWithMostVarsels]['text'] . ", som hadde " . max($catArrayCounter) . " varsler," . $moreOrLessString . $compareVarselCount . " sist måned.";
    if($compareVarselCount < max($catArrayCounter)){
        echo " Her ser vi en negativ trend, da " . $categories[$categoryWithMostVarsels]['text'] . " øker, og vi burde vurdere forebyggende tiltak for å få bukt med problemene.";
    }else{
        echo " Her ser vi en positiv trend, da " . $categories[$categoryWithMostVarsels]['text'] . " minker, og det kan se ut til at tidligere tiltak har fungert.";
    }
}else{
    echo "Ikke nok data til å analysere.";
}
    ?>
</div>
    </div>

