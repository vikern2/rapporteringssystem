<div id="edit-category-div">

    <div class="list-head height_60">
        <div class="head-info">
            <div class="hide-btn"><img class="img-small" src="imgs/close_w.svg"/></div>
        </div>
        <div class="head-info">
            <div class="head-date width_70 larger_text">Kategorier - Trykk for å endre</div>
        </div>
    </div>
    
    <div class="list-feed height_90" id="category_feed">
        <?php foreach ($categories as $type) {?>
            <div class='list-item'>
                <div class="list-content row__title full_width click_change">
                    <?php echo $type['text']; ?>
                </div>
                <div class="list-functions" data-id="<?php echo $type['id']; ?>">
                    <div class="hidden list-fc-update category_update">
                        <img class="list-icon category_update" src="imgs/update.svg" alt="update">
                    </div>
                    <div class="hidden list-fc-edit category_cancel">
                        <img class="list-icon cancel_category_edit" src="imgs/cancel.svg" alt="cancel">
                    </div>
                    <div class="list-fc-delete category_delete">
                        <img class="list-icon category_delete" src="imgs/delete.svg" alt="delete">
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="list-item barrier" id="category_feed__add_row">
                <h2>Trykk for å legge til en kategori</h2>
            <div>
        </div>
    </div>
</div>

<script>

document.addEventListener('click', function(e) {
    const target = e.target;
    

    if (e.target.classList.contains('category_add')) {
        const input = target.parentElement.parentElement.parentElement.getElementsByTagName("input")[0].value;
        
        easyscript.request("../api/public/insertCategory", {"text": input}, "POST", function (response) {
            alert("Kategori lagt til, siden lastes nå på nytt.");
            location.reload();
        });
    }
    else if (e.target.classList.contains('category_delete')) {
        var y = target.parentElement.parentElement.parentElement;
        var index = Array.from(y.parentElement.children).indexOf(y);
        var id = target.parentElement.parentElement.getAttribute('data-id');
        easyscript.request("../api/public/removeCategory", {"id": id}, "POST", function (response) {
            target.parentElement.parentElement.parentElement.parentElement.removeChild(y.parentElement.children[index]);
        });
    }
   
    else if (e.target.classList.contains('category_update')) {
        const input = target.parentElement.parentElement.parentElement.getElementsByTagName("input")[0].value;
        var y = target.parentElement.parentElement.parentElement;
        var index = Array.from(y.parentElement.children).indexOf(y);
        var id = target.parentElement.parentElement.getAttribute('data-id');
        easyscript.request("../api/public/editCategory", {"id": id, "text": input}, "POST", function (response) {
            console.log(response);
            target.parentElement.parentElement.parentElement.children[0].innerHTML = input;
            target.parentElement.parentElement.children[0].classList.add('hidden');
            target.parentElement.parentElement.children[1].classList.add('hidden');
        });
    }
});
</script>
</div>







