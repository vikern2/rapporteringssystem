-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 05. Mar, 2018 09:16 AM
-- Server-versjon: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stud_v17_storøy`
--
CREATE DATABASE IF NOT EXISTS `stud_v17_storøy` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `stud_v17_storøy`;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `activity`
--

CREATE TABLE `activity` (
  `activityid` int(10) UNSIGNED NOT NULL,
  `submitid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `activitycontent` varchar(45) NOT NULL,
  `activitytime` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `categories`
--

CREATE TABLE `categories` (
  `categoryid` int(11) NOT NULL,
  `categorytext` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `categories`
--

INSERT INTO `categories` (`categoryid`, `categorytext`) VALUES
(1, 'Seksuell trakassering'),
(2, 'Avvik'),
(3, 'Tyveri');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `submits`
--

CREATE TABLE `submits` (
  `id` int(11) NOT NULL,
  `submitterid` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `content` text NOT NULL,
  `status` varchar(45) DEFAULT 'IKKE SETT',
  `target` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `submitschange` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `submits`
--

INSERT INTO `submits` (`id`, `submitterid`, `category`, `content`, `status`, `target`, `time`, `submitschange`) VALUES
(7, 1, 1, 'a', 'IKKE SETT', 3, '2018-02-26 11:55:52', NULL),
(8, 1, 1, 'a', 'IKKE SETT', 3, '2018-02-26 11:56:35', NULL),
(13, 1, 2, 'Overgrep', 'IKKE SETT', 2, '2018-02-27 08:08:25', NULL),
(17, 1, 2, 'For dyrt!', 'IKKE SETT', 3, '2018-02-27 11:27:58', NULL),
(18, 1, 1, 'asd', 'IKKE SETT', 3, '2018-02-28 08:47:56', NULL),
(19, 1, 1, 'asd', 'IKKE SETT', 3, '2018-02-28 08:47:58', NULL),
(20, 1, 1, 'asd', 'IKKE SETT', 3, '2018-02-28 08:48:02', NULL),
(25, 2, 2, 'ad', 'IKKE SETT', 2, '2018-02-28 09:22:30', NULL),
(26, 1, 2, 'GRØT TIL 40kr?!?! Scam', 'IKKE SETT', 3, '2018-02-28 19:47:04', NULL),
(27, 1, 1, 'a', 'IKKE SETT', 3, '2018-02-28 19:53:46', NULL),
(30, 1, 3, 'b', 'IKKE SETT', 3, '2018-03-01 10:42:46', NULL),
(32, 1, 1, 'a', 'IKKE SETT', 3, '2018-03-01 12:42:00', NULL),
(33, 1, 1, 'a', 'IKKE SETT', 3, '2018-03-01 12:42:55', NULL),
(35, 1, 2, 'Pat har ikke dusjet', 'IKKE SETT', 3, '2018-03-01 13:32:06', NULL),
(40, 1, 1, 'The Cold War isn\'t thawing; it is burning with a deadly heat. Communism isn\'t sleeping; it is, as always, plotting, scheming, working, fighting.', 'IKKE SETT', 2, '2018-03-02 11:19:06', NULL),
(41, 1, 2, 'hei', 'IKKE SETT', 2, '2018-03-02 11:34:51', NULL),
(42, 2, 1, 'Fredrik kan av og til være litt slimbever', 'IKKE SETT', 5, '2018-03-02 12:32:25', NULL),
(43, 2, 1, 'Fredrik kan av og til være litt slimbever', 'IKKE SETT', 5, '2018-03-02 12:32:27', NULL),
(44, 2, 1, 'Fredrik kan av og til være litt slimbever', 'IKKE SETT', 5, '2018-03-02 12:32:32', NULL),
(45, 2, 1, 'Fredrik kan av og til være litt slimbever', 'IKKE SETT', 5, '2018-03-02 12:32:35', NULL),
(46, 2, 1, 'Fredrik kan av og til være litt slimbever', 'IKKE SETT', 5, '2018-03-02 12:32:40', NULL),
(47, 2, 1, 'Fredrik kan av og til være litt slimbever', 'IKKE SETT', 5, '2018-03-02 12:32:46', NULL),
(48, 2, 1, 'Fredrik kan av og til være litt slimbever', 'IKKE SETT', 5, '2018-03-02 12:32:49', NULL),
(49, 2, 1, 'asdf', 'IKKE SETT', 5, '2018-03-02 12:34:01', NULL),
(50, 2, 3, 'patrik stjal mitt hjerte', 'IKKE SETT', 5, '2018-03-02 12:34:24', NULL),
(51, 2, 1, '123', 'IKKE SETT', 5, '2018-03-02 12:39:51', NULL),
(52, 2, 1, '123', 'IKKE SETT', 5, '2018-03-02 12:39:59', NULL),
(53, 2, 1, '123123', 'IKKE SETT', 5, '2018-03-02 12:45:18', NULL),
(54, 2, 2, '123', 'IKKE SETT', 5, '2018-03-02 12:48:00', NULL),
(55, 2, 1, '123', 'IKKE SETT', 5, '2018-03-02 12:48:21', NULL),
(56, 2, 1, '123', 'IKKE SETT', 5, '2018-03-02 12:50:08', NULL),
(57, 2, 1, '123123123123', 'IKKE SETT', 5, '2018-03-02 12:56:51', NULL),
(58, 2, 2, '123123', 'IKKE SETT', 5, '2018-03-02 12:57:11', NULL),
(59, 1, 2, 'Hei Hei', 'IKKE SETT', 2, '2018-03-05 07:21:04', NULL);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(128) NOT NULL,
  `usertypeid` int(11) NOT NULL DEFAULT '1',
  `agreementseen` tinyint(1) NOT NULL DEFAULT '0',
  `session` varchar(128) DEFAULT NULL,
  `lastlogin` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `users`
--

INSERT INTO `users` (`userid`, `username`, `password`, `usertypeid`, `agreementseen`, `session`, `lastlogin`) VALUES
(1, 'patrik', '$2y$10$5uh27b1dU56FM8hqCyhf2O2EzfNZ4g7Ey99oAvQave8N2GiUyzxwe', 1, 0, 'ses_5a9cf81e8ebd3', '2018-03-05 07:56:14'),
(2, 'Gjertrud', '$2y$10$erWMlLBiZsynV/VEboCeNOePUHjJvb.TikcNT0tz5X13/ZjoDr8UG', 2, 1, 'ses_5a9cf834b5335', '2018-03-05 07:56:36'),
(4, 'bru001', '$2y$10$erWMlLBiZsynV/VEboCeNOePUHjJvb.TikcNT0tz5X13/ZjoDr8UG', 1, 1, 'ses_5a99365a17e07', '2018-03-02 11:32:42'),
(5, 'admin', '$2y$10$5uh27b1dU56FM8hqCyhf2O2EzfNZ4g7Ey99oAvQave8N2GiUyzxwe', 3, 0, 'ses_5a9cf3c41fdb8', '2018-03-05 07:37:40');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `usertype`
--

CREATE TABLE `usertype` (
  `usertypeid` int(11) NOT NULL,
  `typetext` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `usertype`
--

INSERT INTO `usertype` (`usertypeid`, `typetext`) VALUES
(3, 'Admin'),
(2, 'HR'),
(1, 'User');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`activityid`),
  ADD UNIQUE KEY `activityid_UNIQUE` (`activityid`),
  ADD KEY `fk_activity_submits1_idx` (`submitid`),
  ADD KEY `fk_activity_users1_idx` (`userid`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryid`),
  ADD UNIQUE KEY `categoryid_UNIQUE` (`categoryid`);

--
-- Indexes for table `submits`
--
ALTER TABLE `submits`
  ADD PRIMARY KEY (`id`,`submitterid`),
  ADD UNIQUE KEY `idsubmits_UNIQUE` (`id`),
  ADD KEY `fk_submits_users1_idx` (`submitterid`),
  ADD KEY `fk_submits_catebories1_idx` (`category`),
  ADD KEY `fk_submits_usertype1_idx` (`target`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `id_UNIQUE` (`userid`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD KEY `fk_users_usertype_idx` (`usertypeid`);

--
-- Indexes for table `usertype`
--
ALTER TABLE `usertype`
  ADD PRIMARY KEY (`usertypeid`),
  ADD UNIQUE KEY `id_UNIQUE` (`usertypeid`),
  ADD UNIQUE KEY `typetext_UNIQUE` (`typetext`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `submits`
--
ALTER TABLE `submits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `usertype`
--
ALTER TABLE `usertype`
  MODIFY `usertypeid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `activity`
--
ALTER TABLE `activity`
  ADD CONSTRAINT `fk_activity_submits1` FOREIGN KEY (`submitid`) REFERENCES `submits` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_activity_users1` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
