<?php
include 'domain_variables.php';
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo($domain_name)?> </title>
    <link rel="stylesheet" type="text/css" href="style/style.css">
</head>

<body>
    
     <img class="logo-background" src=<?php echo($logo_path) ?> alt="logo">



    <div class="login-page">
        <div class="card-head">
            <h1>Velkommen</h1>
            <h2>Logg inn for å fortsette</h2>
        </div>
        <form class="card-form" method="post" action="api/public/login" id="loginForm">
            <p>Brukernavn</p>
            <input class="wide margin_0_auto" type="text" name="username" placeholder="Brukernavn">
            <p>Passord</p>
            <input class="wide margin_0_auto" type="password" name="password" placeholder="Passord">
            <button class="centered_button btn-img btn-login" type="submit" name="login">Logg inn <img class="img-small" src="imgs/lock-open.svg" alt="log-in"> </button>
        </form>
        <div class="card-footer">
            <p class="message">
                Ikke registrert?
                <a href="http://www.vg.no">Kontakt administrator</a>
            </p>
        </div>
    </div>

    <script src="script/EasyScript.js"></script>
    <script>
        let easyscript = new EasyScript();
        easyscript.checkup(function(response) {
            if (response['status']) {
                easyscript.setSession('id', response['data']['userid']);
                window.location = 'home.php';
            } else {
                document.getElementsByTagName('body')[0].classList.remove('loading');
            }
        });

        easyscript.setupForm(document.getElementById('loginForm'), function() {
            console.log('button clicked');
        }, function(response) {
            if (response['status']) {
                easyscript.setSession('user', response['user']['session']);
                easyscript.setSession('level', response['user']['usertypeid']);
                easyscript.setSession('id', response['user']['userid']);
                window.location = 'home.php';
            } else {
                console.log(response);
                alert(response['message']);
            }
        }, function(element) {
            console.log(element);
            console.log('custom error handler');
        });
    </script>
</body>
</html>


