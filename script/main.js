const easyscript = new EasyScript();

easyscript.checkup(function (response) {
    if (!response['status']) {
        easyscript.logout();
        window.location = 'login.php';
    }
});

let userids = document.getElementsByClassName('popuserid');

for (let i = 0; i < userids.length; i++) {
    userids[i].value = easyscript.getSession('id');
}

document.getElementById('userid').value = easyscript.getSession('id');
const upi = document.getElementById('updatePasswordID');
if (upi != null) {
    upi.value = easyscript.getSession('id');
}

var trans = {
    'content': 'Beskrivelse',
    'cat': 'Kategori',
    'receiver': 'Mottaker',
    'newStatus': 'Status'
}

const userList = document.getElementById('userList');

easyscript.setupForm(document.getElementById('newComplaintForm'), function () {
    console.log('button clicked');

}, function (response) {
    console.log(response);
    alert("Varsel sendt. Det ligger nå under 'Mine Varsler'")
    location.reload();
    /*document.getElementById("submitformCategory").value = "..Trykk for å velge\n";
    document.getElementById("submitformReceiver").value = "..Trykk for å velge\n";
    document.getElementById("submitformTextarea").value = "";*/
}, function (element) {
    alert(trans[element.getAttribute('name')] + ' er påkrevd');
});

document.getElementById('logout').addEventListener('click', function () {
    easyscript.logout();
    window.location = 'login.php';
});

if (document.getElementById('updatePasswordForm') != null) {
    easyscript.setupForm(document.getElementById('updatePasswordForm'), function () {
        console.log('button clicked');

    }, function (response) {
        console.log(response);
        alert("Passord er oppdatert.")
        document.getElementById("passwordModal").remove();
        /*document.getElementById("submitformCategory").value = "..Trykk for å velge\n";
        document.getElementById("submitformReceiver").value = "..Trykk for å velge\n";
        document.getElementById("submitformTextarea").value = "";*/


    }, function (element) {
        alert(trans[element.getAttribute('name')] + ' er påkrevd');
    });

    var password = document.getElementById("pw1"),
        confirm_password = document.getElementById("pw2");

    var validatePassword = function(){
        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passordene er ikke like. Gjør på nytt!");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword();
    confirm_password.onkeyup = validatePassword();
}
easyscript.setupForm(document.getElementById('newUserForm'), function() {

}, function(response) {
    if (response['status']) {
        alert('Bruker lagt til');
    } else {
        alert('Brukernavn eksisterer allerede');
    }
}, function(element) {
    alert("Alle felt er påkrevd");
});

document.addEventListener('click', function (e) {
    const target = e.target;
    if (e.target.classList.contains('admin-category-button') && e.target.classList.contains('add')) {
        const input = target.parentElement.getElementsByTagName("input")[0].value;
        easyscript.request("../api/public/insertCategory", {"text": input}, "POST", function (response) {
            console.log(input);
            location.reload();

        });
    }
    if (e.target.classList.contains('admin-category-button') && e.target.classList.contains('delete')) {
        const id = e.target.getAttribute('data-id');
        easyscript.request("../api/public/removeCategory", {"id": id}, "POST", function (response) {
            console.log(response);
            location.reload();

        });
        console.log(id);
    }
    if (e.target.classList.contains('admin-category-button') && e.target.classList.contains('edit') && !e.target.classList.contains('submit')) {
        target.style.display = "none";
        target.parentElement.children[2].style.display = "none";
        target.parentElement.children[1].style.display = "block";
    }
    if (e.target.classList.contains('admin-category-button') && e.target.classList.contains('edit') && e.target.classList.contains('submit')) {
        const input = target.parentElement.getElementsByTagName("input")[0].value;
        const id = e.target.getAttribute('data-id');
        console.log(input, id);
        easyscript.request("../api/public/editCategory", {"id": id, "text": input}, "POST", function (response) {
            console.log(response);
            location.reload();
        });
    }
});

const filter = {
    normal: document.getElementById('filterType'),
    hr: document.getElementById('filterType_hr'),
    value: document.getElementById('filterValue'),
    hrvalue: document.getElementById('filterValue_hr'),
    clear: document.getElementById('clearFilters'),
    clearhr: document.getElementById('clearFilters_hr')
}

if (filter.normal != null) {
    filter.normal.addEventListener('change', filterType);
    filter.value.addEventListener('change', filterValue);
    filter.clear.addEventListener('click', function(e) {
        filterByType(null, null, e.target.parentElement.parentElement.parentElement.parentElement);
    });
}
if (filter.hr != null) {
    filter.hr.addEventListener('change', filterType);
    filter.hrvalue.addEventListener('change', filterValue);
    filter.clearhr.addEventListener('click', function(e) {
        filterByType(null, null, e.target.parentElement.parentElement.parentElement.parentElement);
    });
}

function filterType(e) {
    let target = e.target;
    let key = target.value;
    let valueContainer = null;
    if (target.getAttribute('id') == 'filterType') {
        valueContainer = document.getElementById('filterValue');
    } else {
        valueContainer = document.getElementById('filterValue_hr');
    }
    let childs = valueContainer.children;

    valueContainer.value = childs[0].value;

    for (let i = 0; i < childs.length; i++) {
        const child = childs[i];
        if (child.classList.contains('option--' + key) || child.classList.contains('donthide')) {
            child.classList.remove('hidden');
        } else {
            child.classList.add('hidden');
        }
    }
}

function filterValue(e) {
    let target = e.target;
    let key = target.value;

    if (target.getAttribute('id') === 'filterType') {
        filterByType(document.getElementById('filterType').value, key, target.parentElement.parentElement.parentElement.parentElement.parentElement);
    } else {
        filterByType(document.getElementById('filterType_hr').value, key, target.parentElement.parentElement.parentElement.parentElement.parentElement);
    } 
}

function filterByType(filter, value, container) {
    container = container.children[1];
    const items = container.children;

    for (let i = 0; i < items.length; i++) {
        let item = items[i];

        if (filter === 'user') {
            if (item.getAttribute('data-receiver') == value) {
                item.classList.remove('hidden');
            } else {
                item.classList.add('hidden');
            }
        } else if (filter === 'category') {
            if (item.getAttribute('data-cat') == value) {
                item.classList.remove('hidden');
            } else {
                item.classList.add('hidden');
            }
        } else if (filter === 'status') {
            if (item.getAttribute('data-status') == value) {
                item.classList.remove('hidden');
            } else {
                item.classList.add('hidden');
            }
        }

        if (filter == null && value == null) {
            item.classList.remove('hidden');
        }
    }
}

const sort = {
    normal: null,
    hr: document.getElementById('sortType_hr'),
    value: null,
    hrvalue: document.getElementById('sortValue_hr')
}

if (sort.normal != null) {

}

if (sort.hr != null) {
    sort.hr.addEventListener('change', null);
    sort.hrvalue.addEventListener('change', sortValue);
}

function sortValue(e) {
    let target = e.target;
    let key = target.value;

    if (target.getAttribute('id') === 'sortValue') {
        sortByType(document.getElementById('sortValue').value, key, target.parentElement.parentElement.parentElement.parentElement.parentElement);
    } else {
        sortByType(document.getElementById('sortValue_hr').value, key, target.parentElement.parentElement.parentElement.parentElement.parentElement);
    } 
}

function sortByType(filter, value, container) {
    container = container.children[1];
    let items = container.children;

    const firstsplit = items[0].getElementsByClassName('list-date')[0].innerText.split('-');
    const firstvalue = parseInt(firstsplit[0]) + parseInt(firstsplit[1]) + parseInt(firstsplit[2]);

    const lastsplit = items[items.length - 1].getElementsByClassName('list-date')[0].innerText.split('-');
    const lastvalue = parseInt(lastsplit[0]) + parseInt(lastsplit[1]) + parseInt(lastsplit[2]);

    let deposit;

    if (filter === 'asc' && !(firstsplit > lastvalue)) {
        deposit = document.createElement('div');
        deposit.classList.add('hidden');
        document.getElementsByTagName('body')[0].appendChild(deposit);

        while (items.length > 0) {
            deposit.appendChild(items[0]);
        }

        items = deposit.children;

        for (let i = items.length - 1; i >= 0; i--) {
            container.appendChild(items[i]);
        }

        deposit.remove();
    } else if (filter === 'desc' && (firstvalue < lastvalue)) {
        deposit = document.createElement('div');
        deposit.classList.add('hidden');
        document.getElementsByTagName('body')[0].appendChild(deposit);

        while (items.length > 0) {
            deposit.appendChild(items[0]);
        }

        items = deposit.children;

        for (let i = items.length - 1; i >= 0; i--) {
            container.appendChild(items[i]);
        }

        deposit.remove();
    }
}

//easyscript.setupForm(document.getElementById('newComplaintForm'), function () {
//    console.log('button clicked');
//
//}, function (response) {
//    console.log(response);
//    alert("Varsel sendt. Det ligger nå under 'Mine Varsler'")
//    location.reload();
//    /*document.getElementById("submitformCategory").value = "..Trykk for å velge\n";
//    document.getElementById("submitformReceiver").value = "..Trykk for å velge\n";
//    document.getElementById("submitformTextarea").value = "";*/
//}, function (element) {
//    alert(trans[element.getAttribute('name')] + ' er påkrevd');
//});

const deleteItemBtn = document.getElementById('deleteComplaintBtn');
if (deleteItemBtn != null) {
    deleteItemBtn.addEventListener('click', function(e) {
        //let parent = e.parentElement.parentElement.parentElement;
        let idinput = document.getElementById("complaint_id");
        let id = idinput.value;

        easyscript.request("../api/public/removeVarsel/" + id, null, "DELETE", function(response) {
            if (response['status']) {
                alert('Varselet er arkivert og vil bli permanent slettet om X tid');
                location.reload();
            }
        });

        //console.log(id);
    });
}

const deleteBtns = document.getElementsByClassName('list-fc-delete');
for (let i = 0; i < deleteBtns.length; i++) {
    if (!deleteBtns[i].classList.contains('category_delete')) {
        deleteBtns[i].addEventListener('click', function(e) {
            let parent = e.target.parentElement.parentElement.parentElement;
            const id = parent.getAttribute('data-id');
            
            easyscript.request("api/public/removeVarsel/" + id, null, "DELETE", function(response) {
                if (response['status']) {
                    alert('Varselet er arkivert og vil bli permanent slettet om X tid');
                    parent.remove();
                    //location.reload();
                }
            });
        });
    } 
}

const changeUsernameForm = document.getElementById('changeUsernameForm');
if (changeUsernameForm != null) {
    easyscript.setupForm(changeUsernameForm, function(){

    }, function(response) {
        if (response['status']) {
            alert('Brukernavnet er oppdatert');
        } else {
            alert(response['message']);
        }
    }, function(element) {
        alert(trans[element.getAttribute('name')] + ' er påkrevd');
    });
}

const changeEmailForm = document.getElementById('changeEmailForm');
if (changeEmailForm != null) {
    easyscript.setupForm(changeEmailForm, function(){

    }, function(response) {
        if (response['status']) {
            alert('Epost er oppdatert');
        } else {
            alert(response['message']);
        }
    }, function(element) {
        alert(trans[element.getAttribute('name')] + ' er påkrevd');
    });
}

const changePasswordForm = document.getElementById('changePasswordForm');
if (changePasswordForm != null) {
    easyscript.setupForm(changePasswordForm, function(){
        
    }, function(response) {
        if (response['status']) {
            alert('Passord er oppdatert');
        } else {
            alert(response['message']);
        }
    }, function(element) {
        alert(trans[element.getAttribute('name')] + ' er påkrevd');
    });
}

const commentForm = document.getElementById('commentForm');
if (commentForm != null) {
    easyscript.setupForm(commentForm, function(){
        
    }, function(response) {
        if (response['status']) {
            alert('Data lagret');
        } else {
            alert(response['message']);
        }
    }, function(element) {
        if (trans[element.getAttribute('name')] == undefined) {
            alert('"' + element.getAttribute('placeholder') + '" er påkrevd');
        } else {
            alert(trans[element.getAttribute('name')] + ' er påkrevd');
        }
    });
}