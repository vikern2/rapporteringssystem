function EasyScript () {
    let _this = this;

    _this.body = document.getElementsByTagName('body')[0];

    _this.loading = function() {
        if (_this.body.classList.contains('loading')) {
            _this.body.classList.remove('loading');
        } else {
            _this.body.classList.add('loading');
        }
    }

    _this.request = function(path, params, method, callback) {
        method = method || 'post';
        method = method.toUpperCase();

        const http = new XMLHttpRequest();
        let httpParams = [];

        for (let key in params) {
            if (!params.hasOwnProperty(key)) {
                continue;
            }

            if (httpParams.length == 0) {
                httpParams.push(key + '=' + params[key]);
            } else {
                httpParams.push('&' + key + '=' + params[key]);
            }
        }

        http.open(method, path, true);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        
        /*let session = _this.getSession('user');

        if (session != null) {
            http.setRequestHeader('session', session);
        }*/

        http.onreadystatechange = function() {
            if (http.readyState === 4 && http.status === 200) {
                //console.log(http.responseText);
                callback(JSON.parse(http.responseText));
            }
        }

        if (method === 'POST') {
            http.send(httpParams.join(''));
        } else {
            http.send(null);
        }
    }

    _this.loadHtml = function(path, callback) {
        _this.loading();

        const http = new XMLHttpRequest();

        http.open('GET', path, true);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        http.onreadystatechange = function() {
            if (http.readyState === 4 && http.status === 200) {
                setTimeout(function() {
                    _this.loading();
                    callback(http.response);
                }, 500);
                
            }
        }

        http.send(null);
    }

    _this.setupForm = function(form, onclick, callback, error) {
        const method = form.getAttribute('method').toUpperCase();
        let url = form.getAttribute('action');
        
        const submitBtn = form.querySelectorAll('[type="submit"]')[0];

        submitBtn.addEventListener('click', function(e) {
            e.preventDefault();

            const inputs = form.getElementsByTagName('input');
            const selects = form.getElementsByTagName('select');
            const textareas = form.getElementsByTagName('textarea');

            onclick();

            let params = [];
            let paramsObject = {};

            for (let i = 0; i < inputs.length; i++) {
                if (inputs[i].getAttribute('type') == 'checkbox') {
                    if (!inputs[i].classList.contains('use_false')) {
                        if (params.length == 0) {
                            console.log(0, inputs[i].getAttribute('name'), inputs[i].checked);
                            params.push(inputs[i].getAttribute('name') + '=' + inputs[i].checked);
                        } else {
                            console.log(1, inputs[i].getAttribute('name'), inputs[i].checked);
                            params.push('&' + inputs[i].getAttribute('name') + '=' + inputs[i].checked);
                        }
                    }

                    paramsObject[inputs[i].getAttribute('name')] = inputs[i].checked;

                    continue;
                }

                let key = inputs[i].getAttribute('name');
                let value = inputs[i].value;

                if (!inputs[i].classList.contains("use_false")) {
                    if (value.length === 0) {
                        if (error === null) {
                            alert(inputs[i].getAttribute('placeholder') + ' is required.');
                        } else {
                            error(inputs[i]);
                        }
                        
                        return;
                    }
                }

                if (params.length == 0) {
                    params.push(key + '=' + value);
                } else {
                    params.push('&' + key + '=' + value);
                }

                paramsObject[key] = value;
            }

            for (let i = 0; i < textareas.length; i++) {
                let key = textareas[i].getAttribute('name');
                let value = textareas[i].value;

                if (!textareas[i].classList.contains('use_false')) {
                    if (value.length === 0) {
                        if (error === null) {
                            alert(textareas[i].getAttribute('placeholder') + ' is required.');
                        } else {
                            error(textareas[i]);
                        }
                        
                        return;
                    }
                }

                if (params.length == 0) {
                    params.push(key + '=' + value);
                } else {
                    params.push('&' + key + '=' + value);
                }

                paramsObject[key] = value;
            }

            for (let i = 0; i < selects.length; i++) {
                let key = selects[i].getAttribute('name');
                let value = selects[i].value;

                if (value.length === 0) {
                    if (error === null) {
                        alert(selects[i].getAttribute('placeholder') + ' is required.');
                    } else {
                        error(selects[i]);
                    }
                    
                    return;
                }

                if (params.length == 0) {
                    params.push(key + '=' + value);
                } else {
                    params.push('&' + key + '=' + value);
                }

                paramsObject[key] = value;
            }

            if (method === 'POST') {
                _this.request(url, paramsObject, method, function(response) {
                    callback(response);
                })
            } else {
                url = url + '?' + params.join;
                _this.request(url, [], method, function(response) {
                    callback(response);
                })
            }
        });
    }

    _this.setSession = function(key, value) {
        const d = new Date();
        d.setTime(d.getTime + 900000);
        const expires = 'expires=' + d.toUTCString();
        document.cookie = key + '=' + value + ';' + expires + ';path=/';
    }

    _this.getSession = function(key) {
        let name = key + '=';
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');

        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            
            if (c.indexOf(key) == 0) {
                let e = c.substring(key.length, c.length);

                if (e.charAt(0) == '=') {
                    e = e.substring(1);
                }

                return e;
            }
        }

        return null;
    }

    _this.checkup = function(callback) {
        const session = _this.getSession('user');
        const userid = _this.getSession('id');
        if (session != null && userid != null) {
            _this.request('api/public/silent', {'session': session, 'userid': userid}, 'POST', callback);
        } else {
            callback({'status': false});
        }
    }

    _this.logout = function() {
        const d = new Date();
        d.setTime(d.getTime);
        const expires = 'expires=' + d.toUTCString();
        document.cookie = 'user=null;' + expires + ';path=/';
        document.cookie = 'id=null;' + expires + ';path=/';
        document.cookie = 'level=null;' + expires + ';path=/';
    }
}