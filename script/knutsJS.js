let container = document.getElementsByClassName('selected-activity')[0];
let deposit = document.getElementsByClassName('deposit')[0];
let actions = document.getElementsByClassName('home-main')[0];
let currentAlertId = 0;


if (document.getElementById("new-form") != null) {
    document.getElementById("new-form").addEventListener("click", function () {
        container.appendChild(document.getElementById('varselCard'));
        if (container.style.visibility === "hidden") {
            container.style.visibility = "visible";
            actions.style.visibility = "hidden";
        } else {
            container.style.visibility = "hidden";
        }
    })
}

if (document.getElementById("statistics") != null) {
    document.getElementById("statistics").addEventListener("click", function () {
        container.appendChild(document.getElementById('statistics-and-reports'));
        if (container.style.visibility === "hidden") {
            container.style.visibility = "visible";
            actions.style.visibility = "hidden";
        } else {
            container.style.visibility = "hidden";
        }
    })
}

if (document.getElementById("main_menu__update_user_data") != null) {
    document.getElementById("main_menu__update_user_data").addEventListener("click", function () {
        container.appendChild(document.getElementById('user_contact_info'));
        if (container.style.visibility === "hidden") {
            container.style.visibility = "visible";
            actions.style.visibility = "hidden";
        } else {
            container.style.visibility = "hidden";
        }
    })
}

if (document.getElementById("main_menu__settings") != null) {
    document.getElementById("main_menu__settings").addEventListener("click", function () {
        var children = document.getElementById("home_menu").children;
        for (i = 0; i < children.length; i++) {
            if (children[i].classList.contains("hidden")) {
                children[i].classList.remove("hidden");
            } else (children[i].classList.add("hidden"));
        }
    })
}

if (document.getElementById("main_menu__settings_exit") != null) {
    document.getElementById("main_menu__settings_exit").addEventListener("click", function () {
        var children = document.getElementById("home_menu").children;
        for (i = 0; i < children.length; i++) {
            if (children[i].classList.contains("hidden")) {
                children[i].classList.remove("hidden");
            } else (children[i].classList.add("hidden"));
        }
    })
}

if (document.getElementById("list-user") != null) {
    document.getElementById("list-user").addEventListener("click", function () {
        if (container.style.visibility === "hidden") {
            container.style.visibility = "visible";
            actions.style.visibility = "hidden";
        } else {
            container.style.visibility = "hidden";
        }
        container.appendChild(document.getElementById('list-user-div'));
    })
}

if (document.getElementById("list-all") != null) {
    document.getElementById("list-all").addEventListener("click", function () {
        container.appendChild(document.getElementById('list-all-div'));
        if (container.style.visibility === "hidden") {
            container.style.visibility = "visible";
            actions.style.visibility = "hidden";
        } else {
            container.style.visibility = "hidden";
        }
    })
}
if (document.getElementById("settings_add_user") != null) {
    document.getElementById("settings_add_user").addEventListener("click", function () {
        container.appendChild(document.getElementById('add_user_div'));
        if (container.style.visibility === "hidden") {
            container.style.visibility = "visible";
            actions.style.visibility = "hidden";
        } else {
            container.style.visibility = "hidden";
        }
    })
}
if (document.getElementById("settings_groups") != null) {
    document.getElementById("settings_groups").addEventListener("click", function () {
        container.appendChild(document.getElementById('register_view'));
        if (container.style.visibility === "hidden") {
            container.style.visibility = "visible";
            actions.style.visibility = "hidden";
        } else {
            container.style.visibility = "hidden";
        }
    })
}
if (document.getElementById("settings_category") != null) {
    document.getElementById("settings_category").addEventListener("click", function () {
        container.appendChild(document.getElementById('edit-category-div'));
        if (container.style.visibility === "hidden") {
            container.style.visibility = "visible";

            actions.style.visibility = "hidden";
        } else {
            container.style.visibility = "hidden";
        }
    })
}
document.addEventListener('click', function (e) {
    let target = e.target;
    if (target.classList.contains('user_view')) {
        target = target.parentElement.parentElement.parentElement;
        show(target);
    } else if (target.classList.contains('admin_view')) {
        document.getElementById("complaint_display").style.display = "none";
        target = target.parentElement.parentElement.parentElement;
        show(target);
    } else if (target.classList.contains('hr_view')) {
        target = target.parentElement.parentElement.parentElement;
        show(target);
    } else if (target.classList.contains('user_edit')) {
        target = target.parentElement.parentElement.parentElement;
        showEdit(target);
    } else if (target.classList.contains('admin-hide-btn') || target.getAttribute('src') === 'imgs/close_w_admin.svg') {
        deposit.appendChild(container.lastChild);
        container.style.visibility = "hidden";
        actions.style.visibility = "visible";
        document.getElementById("sneakDescription").value = "";
        document.getElementById("complaint_display").style.display = "none";
        document.getElementById("sneakPeakForm").style.display = "block";
    } else if (target.classList.contains('hide-btn') || target.getAttribute('src') === 'imgs/close_w.svg') {
        deposit.appendChild(container.lastChild);
        container.style.visibility = "hidden";
        actions.style.visibility = "visible";
    } else if (target.classList.contains('click_change')) {
        var temp = target.innerText;
        var newdiv = document.createElement('input');
        newdiv.setAttribute("type", "text");
        newdiv.setAttribute("placeholder", temp);
        newdiv.setAttribute("name", "cat_new_name");
        target.innerHTML = "<input class='width_70 list-content__input' name='cat_new_name' type='text' placeholder=" + temp + ">"
        var x = target.parentElement.children[1].children[0];
        if (x.classList.contains('hidden')) {
            x.classList.remove('hidden');
            target.parentElement.children[1].children[1].classList.remove('hidden');
        }
    } else if (target.classList.contains('cancel_category_edit')) {
        var y = target.parentElement.parentElement.parentElement;
        var index = Array.from(y.parentElement.children).indexOf(y);
        target.parentElement.parentElement.parentElement.children[0].innerHTML = categoryList[index]['text'];
        target.parentElement.classList.add('hidden');
        target.parentElement.parentElement.children[0].classList.add('hidden');
    } else if (target.classList.contains('cancel_category_add')) {
        var y = target.parentElement.parentElement.parentElement;
        var index = Array.from(y.parentElement.children).indexOf(y);
        target.parentElement.parentElement.parentElement.parentElement.removeChild(y.parentElement.children[index]);
    }
})

function show(element) {
    deposit.appendChild(container.lastChild);
    currentAlertId = element.getAttribute('data-id');
    let box = document.getElementById('view_event_container');
    container.appendChild(box);
    if (document.getElementById('submit-id')) {
        document.getElementById('submit-id').value = element.getAttribute('data-submitid');
    }

    document.getElementById('view_event_container__row_time').innerText = element.getAttribute('data-time');
    document.getElementById('view_event_container__row_category').innerHTML = "<H2>" + element.getAttribute('data-cat') + "</H2>";
    document.getElementById('view_event_container__row_content').innerText = element.getAttribute('data-content');
    document.getElementById('view_event_container__row_status').innerText = element.getAttribute('data-status');
    document.getElementById('complaint_id').value = element.getAttribute('data-id');
    if (document.getElementById('view_event_container__id') != null) {
        document.getElementById('view_event_container__id').value = element.getAttribute('data-id');
    }
    if (element.getAttribute('data-feedback') != "[]") {
        let feedback = "";
        let object = JSON.parse(element.getAttribute('data-feedback'))
        for (const key of object) {
            feedback += key['feedback'] + '<br>';
            console.log(key);        
        }
        document.getElementById('view_event_container__row_user_feedback').innerHTML = feedback;
    }
    
    if (element.getAttribute('data-anon') == 1) {
        document.getElementById('view_event_container__row_submitter').innerText = "Anonym"
    } else {
        document.getElementById('view_event_container__row_submitter').innerText = element.getAttribute('data-submittername');
    }

    if ((element.getAttribute('data-status') == 'Ikke sett')) {
        document.getElementById('deleteComplaintBtn').style.display = 'flex';
    } else {
        document.getElementById('deleteComplaintBtn').style.display = 'none';
    }

    if (document.getElementById('view_event_container__row_hr_feedback') != null) {
        document.getElementById('view_event_container__row_hr_feedback').value = element.getAttribute('data-feedback');
    }

    if (document.getElementById('view_event_container__row_hr_feedback_internal') != null) {
        document.getElementById('view_event_container__row_hr_feedback').value = element.getAttribute('internal-feedback');
    }
    if (element.getAttribute('data-submitterid') != easyscript.getSession('id')) {
        document.getElementById('process_event').classList.remove('hidden');
    } 
}


function showEdit(element) {
    deposit.appendChild(container.lastChild);
    let box = document.getElementById('editVarselCard');
    container.appendChild(box);

    document.getElementById('editid').value = element.getAttribute('data-id');

    const category = document.getElementById('editformCategory');
    const categoryItems = category.getElementsByTagName('option');

    for (let i = 0; i < categoryItems.length; i++) {
        if (categoryItems[i].innerText == element.getAttribute('data-cat')) {
            category.value = categoryItems[i].value;
        }
    }

    const rec = document.getElementById('editformReceiver');
    for (let i = 0; i < receivers.length; i++) {
        if (receivers[i]['groupname'] == element.getAttribute('data-receiver')) {
            rec.value = receivers[i]['id'];
        }
    }

    document.getElementById('editformTextarea').value = element.getAttribute('data-content');
}

var checker = document.getElementById('1');
var sendbtn = document.getElementById('submitForm');
checker.onchange = function () {
    sendbtn.disabled = !this.checked;
};

if (document.getElementById('settings_user') != null) {
    document.getElementById("settings_user").addEventListener("click", function () {
        var options = document.getElementById("user_options");
        var info = document.getElementById("user_list_info");
        var topbar = document.getElementById("user_list_top_bar")
        var headmain = document.getElementById("user-list-head");
        var list = document.getElementById("userList");
        if (options.classList.contains('hidden')) {
            options.classList.remove('hidden');
            list.style.height = "85%";
            headmain.style.height = "15%";
            document.getElementById("user-filter-info").style.height = "50%";
            document.getElementById("user-sort-info").style.height = "50%";
            info.style.height = "25%";
            topbar.style.height = "25%";
        } else {
            options.classList.add('hidden');
            list.style.height = "90%";
            headmain.style.height = "10%";
            info.style.height = "50%";
            topbar.style.height = "50%";
        }
    })
}

if (document.getElementById('settings_hr') != null) {
    document.getElementById("settings_hr").addEventListener("click", function () {
        var options = document.getElementById("hr_options");
        var headmain = document.getElementById("hr-list-head");
        var list = document.getElementById("hrList");
        if (options.classList.contains('hidden')) {
            options.classList.remove('hidden');
            list.style.height = "600px";
            headmain.style.height = "120px";
        } else {
            options.classList.add('hidden');
            list.style.height = "660px";
            headmain.style.height = "60px";
        }
    })
}

if (document.getElementById('process_event') != null) {
    document.getElementById('process_event').addEventListener('click', function () {
        let main_container = document.getElementById('view_event_container');
        let hr_container = document.getElementById('container__hr_view');
        if (hr_container.classList.contains('hidden')) {
            hr_container.classList.remove('hidden');
            let container = main_container.getElementsByClassName('event_container__main')[0];
            document.getElementById('view_event_container__id').value = currentAlertId;
        }
        else {
            hr_container.classList.add('hidden');
        }
    })
}

document.getElementById("category_feed__add_row").addEventListener("click", function () {
    categorylist = document.getElementById('category_feed');
    
    lastItem = document.getElementById('category_feed__add_row');
    //create new row
    var row = document.createElement('div');
    row.classList.add("list-item");
    
    var inputField = document.createElement('div');
    inputField.classList.add("list-content", "row__title", "full_width", "click_change");
    inputField.innerHTML = "<input class='width_80 list-content__input' name='cat_new_name' type='text' placeholder='Navngi kategori'>";
    
    var functions = document.createElement('div');
    functions.classList.add("list-functions");
    
    var update_btn = document.createElement('div');
    update_btn.classList.add("list-fc-update", "category_add");
    update_btn.innerHTML = "<img class='list-icon category_add' src='imgs/update.svg' alt='update'>";
    functions.appendChild(update_btn);
    
    var cancel_btn = document.createElement('div');
    cancel_btn.classList.add("list-fc-edit", "abort");
    cancel_btn.innerHTML = "<img class='list-icon cancel_category_add' src='imgs/cancel.svg' alt='cancel'>";
    functions.appendChild(cancel_btn);
    
    var delete_btn = document.createElement('div');
    delete_btn.classList.add("list-fc-delete", "category_delete", "hidden");
    delete_btn.innerHTML = "<img class='list-icon category_delete' src='imgs/delete.svg' alt='delete'>";
    functions.appendChild(delete_btn);
    
    row.appendChild(inputField);
    row.appendChild(functions);
    categorylist.insertBefore(row, lastItem);
})

document.getElementById("settings_row__change_name").addEventListener("click", function () {
    var cl = document.getElementById("container__change_name").classList;
    if (cl.contains("hidden")) {
        hideUserSettings();
        cl.remove("hidden");
    } else cl.add("hidden");
})

document.getElementById("settings_row__change_email").addEventListener("click", function () {
    var cl = document.getElementById("container__change_email").classList;
    if (cl.contains("hidden")) {
        hideUserSettings();
        cl.remove("hidden");
    } else cl.add("hidden");
})

document.getElementById("settings_row__change_password").addEventListener("click", function () {
    var cl = document.getElementById("container__change_password").classList;
    if (cl.contains("hidden")) {
        hideUserSettings();
        cl.remove("hidden");
    } else {
        cl.add("hidden");
    }
})

function hideUserSettings() {
    var name_container = document.getElementById("container__change_name").classList;
    if (!name_container.contains("hidden")) {
        name_container.add("hidden");
    }
    var email_container = document.getElementById("container__change_email").classList;
    if (!email_container.contains("hidden")) {
        email_container.add("hidden");
    }
    var pw_container = document.getElementById("container__change_password").classList;
    if (!pw_container.contains("hidden")) {
        pw_container.add("hidden");
    }
    var pw1 = document.getElementById('new_password1').classList;
    if (!pw1.contains("hidden")) {
        pw1.add("hidden");
    }
    var pw2 = document.getElementById('new_password2').classList;
    if (!pw2.contains("hidden")) {
        pw2.add("hidden");
    }
}

function sneak() {

    let description = document.getElementById("sneakDescription").value;
    let userid = document.getElementById('sneakUserid').value;
    let submitId = document.getElementById('submit-id').value;
    if (description != "") {
        easyscript.request('../api/public/sneakPeak', {
            'userid': userid,
            'description': description,
            'data-id': submitId
        }, 'POST', function (response) {
            alert('Du sniker nå på et innlegg.');
        });
        document.getElementById("complaint_display").style.display = "block";
        document.getElementById("sneakPeakForm").style.display = "none";
    }else {
        alert('Du må skrive en beskrivelse for å fortsette')
    }
}

function show_gdpr() {
    document.getElementById("gdpr").classList.remove("hidden");
}

document.getElementById("gdpr_close").addEventListener("click", function() {
    document.getElementById("gdpr").classList.add("hidden");
})
