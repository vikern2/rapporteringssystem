<?php
require 'api/public/helper.class.php';
include 'domain_variables.php';
$helper = new Helper();

$categories = $helper->getCategories()['data'];
$recievers = $helper->getReceivers()['data'];
$usertypes = $helper->getUserType()['data'];
$statuses = $helper->getAllStatuses()['data'];
$progresslist = $helper->getProgresses()['data'];

//$usergroups = $helper->getUserGroups()['data'];
$userlevel = $_COOKIE['level'];
$userid = $_COOKIE['id'];
$user = $helper->getUserDetails($userid);

$userspecified = $helper->getUserSpecificVarsel($userid)['data'];
$received = [];

if ($userlevel == 2) {
    $received = $helper->getTargetSpecificVarsler($userlevel, $userid)["data"];
} else if ($userlevel == 3) {
    $received = $helper->getAllVarsler()["data"];
    $userspecified = [];
}

$path = 'widgets/';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo($domain_name) ?></title>
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <link rel="shortcut icon" href="imgs/favicon.ico" type="image/x-icon">
    <link rel="icon" href="imgs/favicon.ico" type="image/x-icon">

</head>

<body>

<img class="logo-background" src=<?php echo($logo_path) ?> alt="logo" style="pointer-events: none ">
<?php

?>
<div class="deposit" style="display:none;">

    <?php
    if ($userlevel > 1) {
        include $path . 'piechart.php';
    }
    include $path . 'update_user_info.php';
    include $path . 'view_complaint.php';
    include $path . 'view_list-feed_user.php';
    include $path . 'view_list-feed_all-complaints.php';
    include $path . 'view_submit_varsel.php';
    include $path . 'view_submit_edit_varsel.php';
    include $path . 'view_register-user.php';
    include $path . 'admin_edit_categorylist.php';
    include 'gdpr.html';
    ?>

</div>

<div class="selected-activity" style="visibility:hidden;">

</div>
<?php
if ($helper->getAgreementSeen($userid)['data']['agreementseen'] == 0) {
    include $path . '/view_firstTimeLogin.php';
}
?>
<div class="home-main" id="home_menu">


    <?php
    include $path . 'home-cards.php';
    ?>

</div>

<script>
    const userspecified = <?php echo json_encode($userspecified); ?>;
    const all = <?php echo json_encode($received); ?>;
    const receivers = <?php echo json_encode($recievers);?>;
    const categoryList = <?php echo json_encode($categories);?>;
</script>
<script src="script/knutsJS.js"></script>
<script src="script/EasyScript.js"></script>
<script src="script/main.js"></script>
</body>

</html>