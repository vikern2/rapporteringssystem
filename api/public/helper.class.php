<?php
// Database
include 'database.class.php';
define('DB_HOST', 'kark.uit.no');
define('DB_USER', 'v17_storøy');
define('DB_PASS', 'storøy');
define('DB_NAME', 'stud_v17_storøy');

include 'user.class.php';
include 'varsel.class.php';
include 'submitscounter.php';
require 'crypto.class.php';

class Helper
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
        if ($this->db->getError() != null) {
            echo $this->db->getError();
            exit();
        }
    }

    function getTextDate($time)
    {
        if ($time < 1) {
            return 'Seconds ago';
        }

        $minutes = ($time % 60);
        $hours = floor($time / 60);
        $days = floor($hours / 24);
        if ($days > 0) {
            if ($days == 1) {
                return $days . ' day ago';
            } else {
                return $days . ' days ago';
            }
        }

        if ($hours > 0) {
            if ($hours == 1) {
                return $hours . ' hour ago';
            } else {
                return $hours . ' hours ago';
            }
        }


        if ($minutes == 1) {
            return $minutes . ' minute ago';
        } else {
            return $minutes . ' minutes ago';
        }
    }

    public function user()
    {
        $this->db->query('SELECT * FROM usertype');

        return ['status' => true, 'data' => $this->db->resultSet()];
    }

    public function getUser($username, $password)
    {
        $this->db->query('SELECT * FROM users WHERE username = :username');
        $this->db->bind(':username', $username);
        $selectedUser = $this->db->single();
        if ($selectedUser != null) {
            $user = new User();
            $user->setUserid($selectedUser['userid']);
            $user->setUsername($selectedUser['username']);
            $user->setSession(uniqid("ses_"));
            $user->setUsertypeid($selectedUser['usertypeid']);
            $user->setAgreementseen($selectedUser['agreementseen']);
            $user->setEmail($selectedUser['email']);
            if (password_verify($password, $selectedUser['password'])) {
                $this->setSession($user->getUserid(), $user->getSession());
                return ['status' => true, 'data' => $user];
            }
        }

        return ['status' => false, 'message' => 'Wrong combination'];
    }
    
    public function silentLogin($session, $userid) {
        $this->db->query('SELECT * FROM users WHERE session = :session AND userid = :userid');
        $this->db->bind(':session', $session);
        $this->db->bind(':userid', $userid);
        $selectedUser = $this->db->single();
        if ($selectedUser != null) {
            $user = new User();
            $user->setUserid($selectedUser['userid']);
            $user->setUsername($selectedUser['username']);
            $user->setSession($session);
            $user->setUsertypeid($selectedUser['usertypeid']);
            $user->setAgreementseen($selectedUser['agreementseen']);
            $user->setEmail($selectedUser['email']);
            return ['status' => true, 'data' => $user];
        }

        return ['status' => false, 'message' => 'Session expired.'];
    }

    public function checkSession($session) {
        return true;
        if ($session == null) {
            return false;
        }
        
        $this->db->query('SELECT userid, username, session FROM users WHERE session = :session');
        $this->db->bind(':session', $session);
        $data = $this->db->single();
        
        $f = fopen("log.txt", "a");

        $date = new DateTime();
        $string = date("Y-m-d H:i:s") . " " . $_SERVER['REMOTE_ADDR'] . " " . json_encode($data) . " " . $session ."\n";

        fwrite($f, $string);
        fclose($f);

        if ($data == null) {
            return false;
        }

        return true;
    }

    public function getUserDetails($userid) {
        $this->db->query('SELECT * FROM users WHERE userid = :userid');
        $this->db->bind(':userid', $userid);
        $selectedUser = $this->db->single();

        if ($selectedUser != null) {
            $user = new User();
            $user->setUserid($selectedUser['userid']);
            $user->setUsername($selectedUser['username']);
            $user->setUsertypeid($selectedUser['usertypeid']);
            $user->setEmail($selectedUser['email']);
        }
        return $user;
    }

    public function setSession($uid, $session)
    {
        $date = new DateTime();
        $this->db->query('UPDATE users SET session = :session, lastlogin = :lastlogin WHERE userid = :uid');
        $this->db->bind(':session', $session);
        $this->db->bind(':lastlogin', date("Y-m-d H:i:s"));
        $this->db->bind(':uid', $uid);
        $this->db->execute();
    }


    public function submitVarsel($userid, $category, $receiver, $content, $anon)
    {
        try {
            $split = explode('_', $receiver);

            $this->db->query('INSERT INTO submits (submitterid, category, content, target, anon, forgroup) VALUES (:userid, :category, :content, :receiver, :anon, :forgroup)');
            $this->db->bind(':userid', $userid);
            $this->db->bind(':category', $category);
            $this->db->bind(':content', $content);
            $this->db->bind(':anon', ($anon == 'true' ? 1 : 0));
            if (sizeOf($split) > 1) {
                $this->db->bind(':receiver', $split[0]);
                $this->db->bind(':forgroup', true);
            } else {
                $this->db->bind(':receiver', $receiver);
                $this->db->bind(':forgroup', false);
            }
            $this->db->execute();
            $this->logActivity($userid,"Made an submit with content: ".$content. " Anon = ". $anon);

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function submitComment($submitid, $userid, $statusid, $content, $feedback)
    {
        try {
            $this->db->query('UPDATE submits SET status = :status WHERE id = :submitid');
            $this->db->bind(':status', $statusid);
            $this->db->bind(':submitid', $submitid);
            $this->db->execute();

            if ($content != null && $content != '') {
                $this->db->query('INSERT INTO comments (submitid, userid, text) VALUES (:submitid, :userid, :text)');
                $this->db->bind(':submitid', $submitid);
                $this->db->bind(':userid', $userid);
                $this->db->bind(':text',$content);
                $this->db->execute();
            }

            if ($feedback != null && $feedback != '') {
                $this->db->query('INSERT INTO feedback (feedback, submittid, writtenBy) VALUES (:feedback, :submitid, :userid)');
                $this->db->bind(':feedback', $feedback);
                $this->db->bind(':submitid', $submitid);
                $this->db->bind(':userid',$userid);
                $this->db->execute();
            }

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function getUserSpecificVarsel($submitterid)
    {
        //$this->db->query('SELECT submits.*, submits.submitterid AS subid, progress.progresstext AS status, categories.categorytext AS category, usertype.typetext AS submitterid, submits.anon AS anon FROM submits JOIN categories ON categories.categoryid = submits.category JOIN usertype ON usertype.usertypeid = submits.target JOIN progress ON submits.status = progress.progressid WHERE submitterid = :submitterid ORDER BY submits.time DESC');
        $this->db->query('SELECT submits.*, submits.submitterid AS subid, progress.progresstext AS status, categories.categorytext AS category, users.username AS submitterid, submits.anon AS anon, submits.time as time FROM submits JOIN categories ON categories.categoryid = submits.category JOIN users ON users.userid = submits.submitterid JOIN progress ON submits.status = progress.progressid WHERE submits.submitterid = :submitterid ORDER BY submits.time DESC');
        $this->db->bind(':submitterid', $submitterid);
        $results = $this->db->resultSet();

        $varselsToReturn = [];
        $index = 0;
        foreach ($results as $result) {
            $this->db->query('SELECT feedback.*, users.username AS username FROM feedback JOIN users ON users.userid = feedback.writtenby WHERE submittid = :subid');
            $this->db->bind(':subid', $result['id']);
            $feedback = $this->db->resultSet();

            $varsel = new Varsel(
                $result['id'],
                $result['subid'],
                $result['submitterid'],
                $result['category'],
                $result['content'],
                $result['status'],
                $result['target'],
                $result['time'],
                $result['submitschange'],
                $result['anon'],
                $feedback);
            $varselsToReturn[$index] = $varsel;
            $index++;
        }
        return ['status' => true, 'data' => $varselsToReturn, 'js' => $results];
    }

    public function getAllVarsler()
    {
        $this->db->query('SELECT submits.*, submits.submitterid AS subid, progress.progresstext AS status, categories.categorytext AS category, users.username AS submitterid, submits.anon AS anon FROM submits JOIN categories ON categories.categoryid = submits.category JOIN users ON users.userid = submits.submitterid JOIN progress ON submits.status = progress.progressid ORDER BY submits.time DESC');
        $results = $this->db->resultSet();

        $varselsToReturn = [];
        $index = 0;
        foreach ($results as $result) {
            $this->db->query('SELECT feedback.*, users.username AS username FROM feedback JOIN users ON users.userid = feedback.writtenby WHERE submittid = :subid');
            $this->db->bind(':subid', $result['id']);
            $feedback = $this->db->resultSet();

            $varsel = new Varsel(
                $result['id'],
                $result['subid'],
                $result['submitterid'],
                $result['category'],
                $result['content'],
                $result['status'],
                $result['target'],
                $result['time'],
                $result['submitschange'],
                $result['anon'],
                $feedback);
            $varselsToReturn[$index] = $varsel;
            $index++;
        }
        return ['status' => true, 'data' => $varselsToReturn];
    }

    public function getFirstVarselTime()
    {
        $this->db->query('SELECT submits.time FROM submits ORDER BY submits.time ASC');
        $results = $this->db->resultSet();

        return ['status' => true, 'data' => $results[0]];
    }

    public function getTargetSpecificVarsler($target, $userid)
    {
        $this->db->query('SELECT submits.*, submits.submitterid AS subid, progress.progresstext AS status, categories.categorytext AS category, users.username AS submitterid, submits.anon AS anon, submits.time as time FROM submits JOIN categories ON categories.categoryid = submits.category JOIN users ON users.userid = submits.submitterid JOIN progress ON submits.status = progress.progressid WHERE (target = :target AND forgroup = true) OR (target = :userid AND forgroup = false) ORDER BY submits.time DESC');
        $this->db->bind(':target', $target);
        $this->db->bind(':userid', $userid);
        $results = $this->db->resultSet();

        $varselsToReturn = array();
        $index = 0;
        foreach ($results as $result) {
            $this->db->query('SELECT feedback.*, users.username AS username FROM feedback JOIN users ON users.userid = feedback.writtenby WHERE submittid = :subid');
            $this->db->bind(':subid', $result['id']);
            $feedback = $this->db->resultSet();

            $varsel = new Varsel(
                $result['id'],
                $result['subid'],
                $result['submitterid'],
                $result['category'],
                $result['content'],
                $result['status'],
                $result['target'],
                $result['time'],
                $result['submitschange'],
                $result['anon'],
                $feedback);
            $varselsToReturn[$index] = $varsel;
            $index++;
        }
        return ['status' => true, 'data' => $varselsToReturn];
        //return ['status' => true, 'data' => $results];
    }

    public function getProgresses()
    {
        $this->db->query('SELECT progressid AS id, progresstext AS text FROM progress');

        return ['status' => true, 'data' => $this->db->resultSet()];

    }

    public function getCategories()
    {
        $this->db->query('SELECT categoryid AS id, categorytext AS text FROM categories');

        return ['status' => true, 'data' => $this->db->resultSet()];
    }

    public function getReceivers()
    {
        $this->db->query('SELECT users.userid AS userid, users.usertypeid AS id, username AS name, usertype.typetext AS groupname FROM users JOIN usertype ON usertype.usertypeid = users.usertypeid WHERE users.usertypeid = 2');

        return ['status' => true, 'data' => $this->db->resultSet()];
    }

    public function getUserType()
    {
        $this->db->query('SELECT usertypeid AS id, typetext AS description FROM usertype');

        return ['status' => true, 'data' => $this->db->resultSet()];
    }

    public function getAllStatuses()
    {
        $this->db->query('SELECT progressid AS id, progresstext AS text FROM progress');

        return ['status' => true, 'data' => $this->db->resultSet()];
    }

    public function removeCategory($id)
    {
        try {
            $this->db->query('DELETE FROM categories WHERE categoryid = :id');
            $this->db->bind(':id', $id);
            $this->db->execute();
            $this->logActivity(5,"Category " .$id." was removed");

            return ['status' => true, $id];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function insertCategory($category)
    {
        try {
            $this->db->query('INSERT INTO categories (categorytext) VALUES (:category)');
            $this->db->bind(':category', $category);
            $this->db->execute();
            $this->logActivity(5,"Category ".$category . " was added to Categories");

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * @return Database
     */
    public function editCategory($categoryid, $newCategoryText)
    {
        $this->db->query('UPDATE categories SET categorytext = :categorytext WHERE categoryid = :categoryId');
        $this->db->bind(':categorytext', $newCategoryText);
        $this->db->bind(':categoryId', $categoryid);
        $this->db->execute();
        $this->logActivity(5,"Edited category ".$categoryid ." to ". $newCategoryText);

        return ['status' => true];
    }

    public function editVarsel($content, $id, $cat, $receiver)
    {
        $date = new DateTime();
        try {
            $this->db->query('UPDATE submits SET content = :content, category = :cat, target = :receiver, submitschange = :changed WHERE id = :id');
            $this->db->bind(':content', $content);
            $this->db->bind(':cat', $cat);
            $this->db->bind(':receiver', $receiver);
            $this->db->bind(':id', $id);
            $this->db->bind(':changed', date("Y-m-d H:i:s"));
            $this->db->execute();

            $this->logActivity(-1,"Submit: " . $id." was edited. Content: ".$content);

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function newUser($username, $password, $usertypeid)
    {
        $password = password_hash($password, PASSWORD_DEFAULT);
        try {
            $this->db->query('INSERT INTO users (username, password, usertypeid) VALUES (:username, :password, :usertypeid)');
            $this->db->bind(':username', $username);
            $this->db->bind(':password', $password);
            $this->db->bind(':usertypeid', $usertypeid);
            $this->db->execute();

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function getAgreementSeen($userid)
    {
        $this->db->query('SELECT agreementseen FROM users WHERE userid = :userid');
        $this->db->bind('userid', $userid);
        $results = $this->db->single();
        return ['status' => true, 'data' => $results];
    }

    public function firstTimeLogin($userid, $password)
    {
        $password = password_hash($password, PASSWORD_DEFAULT);
        try {
            $this->db->query('UPDATE users SET password = :password, agreementseen =1 WHERE userid = :userid');
            $this->db->bind(':password', $password);
            $this->db->bind(':userid', $userid);
            $this->db->execute();

            $this->logActivity($userid,"logged in for the first time");

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }

    }

    public function deleteVarsel($id)
    {
        try {
            $this->db->query('INSERT INTO deletedsubs (submitterid, category, content, status, target, time, submitschange) SELECT submitterid, category, content, status, target, time, submitschange FROM submits WHERE id = :id');
            $this->db->bind(':id', $id);
            $this->db->execute();

            $this->db->query('DELETE FROM submits WHERE id = :sid');
            $this->db->bind(':sid', $id);
            $this->db->execute();

            $this->logActivity(-1,"changed password");

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }


    public function getCategorySpecificVarselCount($category)
    {
        $this->db->query('SELECT categories.categorytext AS category FROM submits JOIN categories ON categories.categoryid = submits.category WHERE category = :category');
        $this->db->bind(':category', $category);
        $results = $this->db->resultSet();

        if ($results != null) {
            $submitscounter = new Submitscounter(
                $results[0]['category']
            );

            $numberOfSubmits = 0;

            foreach ($results as $result) {
                $numberOfSubmits++;
            }

            $submitscounter->setNumberOfSubmits($numberOfSubmits);

            return $submitscounter;
        }
    }

    public function submitFeedback($submitid, $userid, $feedback)
    {
        try {
            $this->db->query('INSERT INTO feedback (submittid, writtenby,feedback) VALUES (:submitid,:writtenby,:feedback)');
            $this->db->bind(':submitid', $submitid);
            $this->db->bind(':writtenby', $userid);
            $this->db->bind(':feedback', $feedback);

            $this->db->execute();
            $this->logActivity($userid,"submitted feedback");


            // $this->db->query('INSERT INTO comments (submitid, userid, text) VALUES (:submitid, :userid, :text)');
            // $this->db->bind(':submitid', $submitid);
            // $this->db->bind(':userid', $userid);
            // $this->db->bind(':text',$content);
            // $this->db->execute();

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }


    public function getMonthSpecificVarsler($month)
    {
        try {
            $this->db->query('SELECT * FROM submits WHERE month(time) = :month');
            $this->db->bind(':month', $month);
            $results = $this->db->resultSet();

            if ($results != null) {
                return ['status' => true, 'data' => $results];
            }
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function getMonthCategorySpecificVarselCount($category, $month)
    {
        $this->db->query('SELECT categories.categorytext AS category FROM submits JOIN categories ON categories.categoryid = submits.category WHERE category = :category AND month(time) = :month');
        $this->db->bind(':category', $category);
        $this->db->bind(':month', $month);
        $results = $this->db->resultSet();

        if ($results != null) {
            $submitscounter = new Submitscounter(
                $results[0]['category']
            );

            $numberOfSubmits = 0;

            foreach ($results as $result) {
                $numberOfSubmits++;
            }

            $submitscounter->setNumberOfSubmits($numberOfSubmits);

            return $submitscounter;
        }
    }

    public function changeUsername($userid, $username) {
        try {
            $this->db->query('UPDATE users SET username = :username WHERE userid = :userid');
            $this->db->bind(':username', $username);
            $this->db->bind(':userid', $userid);
            $this->db->execute();

            $this->logActivity($userid,"changed username");

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function changeEmail($userid, $email) {
        try {
            $this->db->query('UPDATE users SET email = :email WHERE userid = :userid');
            $this->db->bind(':email', $email);
            $this->db->bind(':userid', $userid);
            $this->db->execute();

            $this->logActivity($userid,"changed email");

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function changePassword($userid, $old, $new, $verify) {
        try {
            if ($new != $verify) {
                return ['status' => false, 'message' => 'Ulike passord'];
            }

            $this->db->query('SELECT password FROM users WHERE userid = :userid');
            $this->db->bind(':userid', $userid);

            $data = $this->db->single();

            if (!password_verify($old, $data['password'])) {
                return ['status' => false, 'message' => 'Gammelt passord matcher ikke'];
            }

            $this->db->query('UPDATE users SET password = :pw WHERE userid = :userid');
            $this->db->bind(':pw', password_hash($new, PASSWORD_DEFAULT));
            $this->db->bind(':userid', $userid);
            $this->db->execute();
            $this->logActivity($userid,"changed password");
            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function sneak($userId, $description, $submitId)
    {
        try {
            $descriptiontext = "SNEAKED: " . $description;
            $this->db->query('INSERT INTO log (uid,description,submitid) VALUES (:uid,:description,:submitid)');
            $this->db->bind(':uid', $userId);
            $this->db->bind(':description', $descriptiontext);
            $this->db->bind(':submitid', $submitId);
            $this->db->execute();

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];

        }
    }
    private function logActivity($userId = -1, $description){ // uid -1 means userId is irrelevant
        try {
            $this->db->query('INSERT INTO log (uid,description) VALUES (:uid,:description)');
            $this->db->bind(':uid', $userId);
            $this->db->bind(':description', $description);
            $this->db->execute();

            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];

        }
    }

    public function submitStatusChange($submitid, $statusid)
    {
        try {
            $this->db->query('UPDATE submits SET status = :status WHERE id = :submitid');
            $this->db->bind(':status', $statusid);
            $this->db->bind(':submitid', $submitid);
            $this->db->execute();
			
            return ['status' => true];
        } catch (Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    private function mail($someid, $type) {
        // type = 0: New Varsel, type = 1: Feedback
        if ($type == 0) {
            $split = explode('_', $someid);
            $emails = [];

            if (sizeOf($split) > 1) {
                $this->db->query('SELECT email FROM users WHERE usertypeid = :uid');
                $this->db->bind(':uid', $split[0]);
                $data = $this->db->resultSet();

                foreach ($data as $item) {
                    array_push($emails, $item['email']);
                }
            } else {
                $this->db->query('SELECT email FROM users WHERE userid = :uid');
                $this->db->bind(':uid', $split[0]);

                $data = $this->db->single();

                array_push($emails, $data['email']);
            }

            foreach ($emails as $mitem) {
                $fields = [
                    'to' => $mitem,
                    'subject' => 'Nytt varsel',
                    'content' => 'Du eller din brukergruppe har mottatt et nytt varsel.'
                ];

                $this->curlMail($fields);
            }
        } elseif ($type == 1) {
            $this->db->query('SELECT users.email FROM submits JOIN users ON submits.submitterid = users.userid WHERE id = :sid');
            $this->db->bind(':sid', $someid);
            $data = $this->db->single();

            $fields = [
                'to' => $data['email'],
                'subject' => 'Nytt varsel',
                'content' => 'Tilbakemelding på et varsel mottatt.'
            ];

            $this->curlMail($fields);
        }
    }


    private function curlMail($fields) {
        $url = 'https://kark.uit.no/~fst034/mail.php';

        $fields_string = '';
        //url-ify the data for the POST
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);
    }
}

?>