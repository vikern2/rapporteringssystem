<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

if (ini_get('date.timezone') == '') {
    date_default_timezone_set('UTC');
}

include 'helper.class.php';
$helper = new Helper();

require __DIR__ . '/../vendor/autoload.php';

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);
$app->add(new \Slim\middleware\Session([
    'name' => 'diamond_session',
    'autorefresh' => true,
    'lifetime' => '1 hour'
]));

$container = $app->getContainer();
$container['session'] = function ($c) {
    return new \SlimSession\Helper;
};

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

/**
 * Example of GET-REQUEST to list out all roles
 * 
 * @param $req reqest
 * @param $res response
 * @param $args arguments from get url
 * @param $app default, just have it there
 * @param $helper makes the helper class available in the function
 */

/*$app->get('/get/{test}', function($req, $res, $args) use ($app, $helper) {
    $arg = $args['test'];
    echo "ARGUMENT: {$arg}";
    return json_encode($helper->user());
});*/

/**
 * Example of POST-REQUEST
 * 
 * @param $req reqest
 * @param $res response
 * @param $app default, just have it there
 * @param $helper makes the helper class available in the function
 */


$app->post('/post', function($req, $res) use ($app, $helper) {
    $postdata = json_decode(file_get_contents('php://input'), true);
    $arg = $postdata['key'];
    echo "ARGUMENT: {$arg}";
    return json_encode($helper->user());
});

// Run app
$app->run();
