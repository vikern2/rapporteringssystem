<?php

class User implements \JsonSerializable {
    private $userid;
    private $username;
    private $session;
    private $usertypeid;
    private $agreementseen;
    private $email;


    public function __constructor() {

    }

    public function getUserid()
    {
        return $this->userid;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setUserid($userid)
    {
        $this->userid = $userid;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getSession()
    {
        return $this->session;
    }

    public function setSession($session)
    {
        $this->session = $session;
    }

    public function getUsertypeid()
    {
        return $this->usertypeid;
    }

    public function setUsertypeid($usertypeid)
    {
        $this->usertypeid = $usertypeid;
    }

    public function getAgreementseen()
    {
        return $this->agreementseen;
    }

    public function setAgreementseen($agreementseen)
    {
        $this->agreementseen = $agreementseen;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}