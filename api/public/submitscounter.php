<?php

class submitscounter implements \JsonSerializable{
    private $categoryText;
    private $numberOfSubmits;

    public function setNumberOfSubmits($numberOfSubmits)
    {
        $this->numberOfSubmits = $numberOfSubmits;
    }

    public function __construct($categoryText)
    {
        $this->categoryText = $categoryText;
    }

    public function getCategoryText()
    {
        return $this->categoryText;
    }

    public function getNumberOfSubmits()
    {
        return $this->numberOfSubmits;
    }


    function jsonSerialize()
    {
        return get_object_vars($this);
    }
}