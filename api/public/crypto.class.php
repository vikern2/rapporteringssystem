<?php
class Crypto {
    private $characters = ["a", "A", "b", "B", "c", "C", "d", "D", "e", "E", "f", "F", "g", "G", "h", "H", "i", "I", "j", "J", "k", "K", "l", "L", "m", "M", "n", "N", "o", "O", "p", "P", "q", "Q", "r", "R", "s", "S", "t", "T", "u", "U", "v", "V", "w", "W", "x", "X", "y", "Y", "z", "Z", /*"æ", "Æ", "ø", "Ø", "å", "Å",*/ "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    private $array = null;
    private $indexes = null;
    private $fixed = false;

    public function __constructor() {
        
    }

    public function buildUp($salt) {
        $index = $this->getStartIndex($salt);

        $this->array = [];
        $this->indexes = [];

        $ii = 0;
        for ($i = $index; $i < sizeof($this->characters); $i++) {
            array_push($this->array, $this->characters[$i]);
            $this->indexes[$this->characters[$i]] = $ii;
            $ii++;
        }

        $ii = $index;
        for ($i = $index; $i > 0; $i--) {
            array_push($this->array, $this->characters[$i]);
            $this->indexes[$this->characters[$i]] = $ii;
            $ii--;
        }
    }

    public function encrypt($string) {
        $output = "";

        for ($i = 0; $i < strlen($string); $i++) {
            $charindex = array_search($string[$i], $this->characters, true);
            if ($charindex != false) {
                $output .= $this->array[$charindex];
            } else {
                $output .= $string[$i];
            }
        }

        return $output;
    }

    public function decrypt($string) {
        $output = "";

        for ($i = 0; $i < strlen($string); $i++) {
            $str = $string[$i];
            $charindex = array_search($str, $this->array, true);

            if ($charindex != false) {
                $output .= $this->characters[$charindex];
            } else {
                $output .= $str;
            }
        }

        return $output;
    }

    public function getValue($needle) {
        $value = 0;

        for ($i = 0; $i < strlen($needle); $i++) {
            $value += array_search($needle[$i], $this->characters);
        }

        return $value;
    }

    private function unifySalt($salt) {
        return strtolower(str_replace(" ", "", $salt));
    }

    private function getStartIndex($salt) {
        $value = $this->getValue($this->unifySalt($salt));
        $index = substr(sizeof($this->characters) / $value, 2, 2);
        $redo = 0;

        if ($index >= sizeof($this->characters)) {
            return $this->getStartIndex(ceil($index/2));
        } else {
            return $index;
        }
    }
}
?>