<?php

class Varsel implements \JsonSerializable
{
    private $id;
    private $submitterid;
    private $submittername;
    private $category;
    private $content;
    private $status;
    private $target;
    private $time;
    private $submitschange;
    private $anon;
    private $feedback;


    public function __construct($id, $submitterid, $submittername, $category, $content, $status, $target, $time, $submitschange, $anon, $feedback = null)
    {
        $this->id = $id;
        $this->submitterid = $submitterid;
        $this->submittername = $submittername;
        $this->category = $category;
        $this->content = $content;
        $this->status = $status;
        $this->target = $target;
        $this->time = $time;
        $this->submitschange = $submitschange;
        $this->anon = $anon;
        $this->feedback = $feedback;
    }


    public function getFeedback()
    {
        return $this->feedback;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getSubmittername() {
        return $this->submittername;
    }

    public function setSubmittername($submittername)
    {
        $this->submittername = $submittername;
    }

    public function getSubmitterid()
    {
        return $this->submitterid;
    }

    public function setSubmitterid($submitterid)
    {
        $this->submitterid = $submitterid;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getTarget()
    {
        return $this->target;
    }

    public function setTarget($target)
    {
        $this->target = $target;
    }

    public function getTime()
    {
        return  $this->time;
    }

    public function setTime($time)
    {
        $this->time = $time;
    }

    public function getSubmitschange()
    {
        return $this->submitschange;
    }

    public function setSubmitschange($submitschange)
    {
        $this->submitschange = $submitschange;
    }

    public function getAnon() {
        return $this->anon;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}