<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->post('/test', function($req, $res) use ($app, $helper) {
    return json_encode($helper->user());
});

$app->post('/login', function($req, $res) use ($app, $helper) {
	$data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

	$loginUser = $helper->getUser($data['username'], $data['password']);

    if ($loginUser['status']) {
        $agreementseen = $loginUser['data']->getAgreementseen();
        //if ($agreementseen == 1) {
            //$this->session->set($loginUser['data']->getSession(), $loginUser['data']);
            return json_encode(['status' => true, 'user' => $loginUser['data']]);
        //} else {
            //return json_encode(['status' => false, 'message' => 'You need to see the agreement!?']);
        //}
    }

    return json_encode($loginUser);
});

$app->post('/silent', function($req, $res) use ($app, $helper) {
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    /*if ($this->session->exists($data['session'])) {
        return json_encode(['status' => true, 'data' => $this->session->get($data['session'])]);
    }*/

    //return json_encode(['status' => false, 'message' => 'session expired.']);
    return json_encode($helper->silentLogin($data['session'], $data['userid']));
});

$app->post('/newVarsel', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->submitVarsel($data['userid'], $data['cat'], $data['receiver'], $data['content'], $data['anon']));
});

$app->post('/submitComment', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->submitComment($data['data-id'], $data['userid'], $data['newStatus'], $data['intern'], $data['kommentarer']));
});

$app->get('/get/{submitterid}', function($req, $res, $args) use ($app, $helper) {
    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

	return json_encode($helper->getUserSpecificVarsel($args['submitterid']));
});

$app->get('/allVarsler', function($req, $res, $args) use ($app, $helper){
    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

	return json_encode($helper->getAllVarsler());
});

$app->get('/getCategories', function($req, $res, $args) use ($app, $helper){
    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->getCategories());
});

$app->get('/submitsCount/{category}', function($req, $res, $args) use ($app, $helper){
    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    $count = $helper->getCategorySpecificVarselCount($args['category']);
    return json_encode($count);
});

$app->get('/targetVarsler/{target}/{userid}', function($req, $res, $args) use ($app, $helper){
    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

	$varsler = $helper->getTargetSpecificVarsler($args['target'], $args['userid']);
	return json_encode($varsler);
});

$app->post('/newCategory', function($req, $res) use ($app, $helper) {
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->insertCategory($data['category']));
});


$app->delete('/removeCategory/{id}', function($req, $res, $args) use ($app, $helper){
    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->removeCategory($args['id']));
});

$app->post('/editVarsel', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->editVarsel($data['content'], $data['id'], $data['cat'], $data['receiver']));
});

$app->post('/newUser', function($req, $res) use ($app, $helper) {
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    $data['password'] = "metoo";
    return json_encode($helper->newUser($data['username'], $data['password'], $data['usertypeid']));
});
$app->post('/firstTimeLogin', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->firstTimeLogin($data['userid'], $data['password']));
});
$app->post('/removeCategory', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->removeCategory($data['id']));
});
$app->post('/editCategory', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->editCategory($data['id'],$data['text']));
});
$app->post('/insertCategory', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->insertCategory($data['text']));
});

$app->delete('/removeVarsel/{id}', function($req, $res, $args) use ($app, $helper){
    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->deleteVarsel($args['id']));
});
$app->post('/submitFeedback', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->submitFeedback($data['data-id'], $data['userid'], $data['feedback']));
    //  'data-id'   =>  Varsel det gjelder
    //  'user-id'   =>  Hvem skrev tilbakemelding
    //  'feedback'  =>  Tilbakemelding
});

$app->post('/changeUsername', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->changeUsername($data['userid'], $data['new_username']));
});

$app->post('/changeEmail', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->changeEmail($data['userid'], $data['new_email']));
});

$app->post('/changePassword', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->changePassword($data['userid'], $data['old_password'], $data['new_password'], $data['new_password_confirm']));
});

$app->post('/sneakPeak', function($req, $res) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->sneak($data['userid'], $data['description'], $data['data-id']));
});

$app->get('/getAllStatuses', function($req, $res, $args) use ($app, $helper){
    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->getAllStatuses());
});

$app->get('/getReceivers', function($req, $res, $args) use ($app, $helper){
    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->getReceivers());
});

$app->post('/submitStatusChange', function($req, $res, $args) use ($app, $helper){
    $data = (json_decode(file_get_contents('php://input'), true) == '' ? $req->getParsedBody() : json_decode(file_get_contents('php://input'), true));

    if (!$helper->checkSession(getSessionFromHeaders($req->getHeaders()))) {
        return json_encode(['status' => false, 'message' => 'Session expired']);
    }

    return json_encode($helper->submitStatusChange($data['submitid'], $data['statusid']));
});


function getSessionFromHeaders($headers) {
    foreach ($headers as $name => $values) {
        if ($name == 'HTTP_SESSION') {
            return implode(", ", $values);
        }
    }

    return null;
}